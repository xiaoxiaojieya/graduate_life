import torch
import torchvision
from myDataset import myMPIIDataset as myDataset
from model import myUnipose
from evaluate import accuracy
import numpy as np

from tqdm import tqdm
from colorama import Fore

numClasses = 16

Train_BatchSize = 6
Train_Num_worker = 6
train_dataset = myDataset(root_dir="../datasets/MPIII/", sigma=3, is_train=True, transform=None)
train_loader = torch.utils.data.DataLoader(dataset=train_dataset, batch_size=Train_BatchSize, shuffle=True, num_workers=Train_Num_worker)
print("len(train_loader) : ",len(train_loader))

Test_BatchSize = 2
Test_Num_worker = 2
test_dataset = myDataset(root_dir="../datasets/MPIII/", sigma=3, is_train=False, transform=None)
test_loader = torch.utils.data.DataLoader(dataset=test_dataset, batch_size=Test_BatchSize, shuffle=True, num_workers=Test_Num_worker)
print("len(test_loader) : ",len(test_loader))

model = myUnipose(num_classes=numClasses, output_stride=16).cuda()
model.load_state_dict(torch.load('./weight/' + 'unipose__epoch-10_mAP-0.5432640139187653_best.pth'))
print(model)

MSELoss = torch.nn.MSELoss().cuda()

lr = 0.0000001
optimizer = torch.optim.Adam(model.parameters(), lr=lr)

epochs = 50
isBest = 0.543
for epoch in range(epochs):

    train_loss = 0.0

    train_tbar = tqdm(train_loader, bar_format='{l_bar}%s{bar}%s{r_bar}' % (Fore.CYAN, Fore.MAGENTA) )

    model.train()
    for i, (input, heatmap, centermap, img_path) in enumerate(train_tbar):
        input = input.cuda()
        heatmap = heatmap.cuda()

        optimizer.zero_grad()

        # 得到heatmap
        heat = model(input)
        # loss计算
        loss_heat = MSELoss(heat, heatmap)

        loss = loss_heat

        train_loss += loss_heat.item()

        # 针对loss后向传播
        loss.backward()
        optimizer.step()

        train_tbar.set_description('Train loss: %.6f' % (train_loss / ((i + 1) * Train_BatchSize)))

    print("Train ： [Epoch: ", epoch, "/", epochs, " ] || ", " avgloss: ",train_loss/len(train_dataset))

    # =========================================================================

    val_loss = 0.0  # 测试的loss值记录
    AP = np.zeros(numClasses + 1)  # 长度为 16+1 的list.
    count = np.zeros(numClasses + 1)  # 各个关键点上的统计过多少数据。 用于计算平均。

    mAP = 0.0
    test_tbar = tqdm(test_loader, bar_format='{l_bar}%s{bar}%s{r_bar}' % (Fore.MAGENTA, Fore.CYAN) )
    model.eval()
    for i, (input, heatmap, centermap, img_path) in enumerate(test_tbar):
        input = input.cuda()
        heatmap = heatmap.cuda()

        optimizer.zero_grad()
        heat = model(input)

        # 记录测试的loss
        loss_heat = MSELoss(heat, heatmap)
        val_loss += loss_heat.item()

        # 送入预测heat 与 标签heatmap 进行 mAP 计算.
        acc, cnt, pred, visible = accuracy(heat.detach().cpu().numpy(), heatmap.detach().cpu().numpy())
        # print(acc.shape, cnt, pred.shape, visible.shape)  # (17,) 16 (2, 17, 2) (17,)
        """
            acc: [17,]  0位置表示的是16个关键点的平均准确率， 1～16表示的是16个关键点的准确率。
            cnt： 实数 表示关键点可见的数量
            pred： [batch, 17, 2] , 关键点的位置。
            visible： [17,] ， 记录下指定位置的点是否可见， 0表示不可见。
        """

        """
            当前batch中的所有关键点的平均准确率计算出来之后，接下来就应该计算各个点的map。
            mAP： mean Average Precision, 即各类别 平均准确率 的平均值。
            所以，每次AP累计计算的时候，首先需要加载出之前计算出的 平均AP值， （AP*之前数量 + 当前均值）/当前数量。
        """
        # 0位置单独计算，其实没什么用。
        AP[0] = (AP[0] * i + acc[0]) / (i + 1)
        # 累计计算所有点各自的 平均准确率。
        for j in range(1, numClasses + 1):
            # 当前点是可见点才可以参与累计。
            if visible[j] == 1:
                # AP[j] * count[j]：加载出前边计算过的点的整体准确率， acc[j]当前准确率，  / (count[j] + 1)得到当前平均准确率。
                AP[j] = (AP[j] * count[j] + acc[j]) / (count[j] + 1)
                # 当前点统计过了，所以参与计算点+1
                count[j] += 1

        # mAP： mean Average Precision, 即各类别 平均准确率 的平均值。
        mAP = AP[1:].sum() / (numClasses)  # 求 mAP

        test_tbar.set_description('Test loss: %.6f' % (val_loss / ((i + 1) * Test_BatchSize)))

    print("Test ： [Epoch: ", epoch, "/", epochs, " ] || ", "mAP : ", mAP ," || avgloss: ", val_loss / len(test_loader))

    # 保存权重
    if mAP > isBest:
        isBest = mAP
        torch.save(model.state_dict(), "./weight/unipose_" + "_epoch-" + str(epoch) + "_mAP-" + str(isBest) + '_best.pth')
        print("************** Saving... **************")


