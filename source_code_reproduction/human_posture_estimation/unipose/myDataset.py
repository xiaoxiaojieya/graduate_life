import torch
import numpy as np
import torch.utils.data as data
import json
import cv2

import torchvision.transforms as transforms


"""
    高斯热度图:就是在每个点上叠加高斯分布.
"""
def guassian_kernel(size_w, size_h, center_x, center_y, sigma):

    gridy, gridx = np.mgrid[0:size_h, 0:size_w]

    D2 = (gridx - center_x) ** 2 + (gridy - center_y) ** 2

    return np.exp(-D2 / 2.0 / sigma / sigma)


class myMPIIDataset(data.Dataset):

    def __init__(self, root_dir, sigma, is_train, transform=None):
        self.width = 368  # 输入的尺寸
        self.height = 368

        self.transformer = transform  # 图像处理器
        self.is_train = is_train  # 是否训练
        self.sigma = sigma  # heatmap的参数
        self.parts_num = 16  # 关键点数量
        self.stride = 8  # 布距

        # 根路径
        self.labels_dir = root_dir
        self.images_dir = root_dir + "images/"

        self.videosFolders = {}  #
        self.labelFiles = {}
        self.full_img_List = {}
        self.numPeople = []

        # 加载注释文件。
        with open(self.labels_dir+"mpii_annotations.json") as anno_file:
            self.anno = json.load(anno_file)
        # print("len(self.anno) : ", len(self.anno))  # 25204

        """
            上一步加载到的 self.anno 文件拥有 25204 个标注信息，每个标注是 字典形式。
            train_list与val_list 里边的内容是 self.anno 文件中的 index。
        """
        self.train_list = []  # 训练 lsit
        self.val_list = []  # 测试 list
        # 区分出训练数据与测试数据。
        for idx, val in enumerate(self.anno):
            if val['isValidation'] == True:
                self.val_list.append(idx)
            else:
                self.train_list.append(idx)

        # 得到 训练/测试 数据的list。
        if is_train:
            self.img_List = self.train_list
        else:
            self.img_List = self.val_list

    def __getitem__(self, index):

        # 由index索引出img_List中的index，然后确定出 self.anno 中对应坐标的字典。
        current_infoemation = self.anno[self.img_List[index]]  # 当前图片的注释字典。
        # print(current_infoemation)
        """
            {'dataset': 'MPI', 'isValidation': 0.0, 'img_paths': '056179785.jpg', 
            'img_width': 1280.0, 'img_height': 720.0, 'objpos': [809.0, 341.0], 
            'joint_self': [[486.0, 610.0, 1.0], [598.0, 530.0, 1.0], [704.0, 415.0, 1.0], [719.0, 412.0, 0.0], [638.0, 511.0, 0.0], [524.0, 587.0, 0.0], [712.0, 414.0, 0.0], [892.0, 241.0, 0.0], [890.234, 243.169, 1.0], [963.766, 152.831, 1.0], [1083.0, 113.0, 1.0], [981.0, 184.0, 1.0], [864.0, 235.0, 1.0], [920.0, 247.0, 0.0], [1020.0, 194.0, 1.0], [1081.0, 120.0, 0.0]], 
            'scale_provided': 3.494, 
            
            'joint_others': [[677.0, 612.0, 1.0], [688.0, 458.0, 0.0], [658.0, 349.0, 1.0], [722.0, 346.0, 0.0], [770.0, 436.0, 0.0], [764.0, 561.0, 1.0], [690.0, 348.0, 0.0], [738.0, 210.0, 1.0], [742.682, 183.643, 1.0], [760.318, 84.356, 1.0], [714.0, 355.0, 1.0], [666.0, 298.0, 1.0], [682.0, 211.0, 1.0], [794.0, 208.0, 1.0], [801.0, 285.0, 0.0], [817.0, 327.0, 0.0]], 
            'scale_provided_other': 3.025, 'objpos_other': [768.0, 340.0], 'annolist_index': 14480.0, 'people_index': 1.0, 'numOtherPeople': 1.0}
        """


        img_path = self.images_dir + current_infoemation['img_paths']  # 当前图片的路径。
        # print(img_path)

        # points = torch.Tensor(current_infoemation['joint_self'])  # 当前图片的关键点
        points = current_infoemation['joint_self']  # 当前图片的关键点
        """
            0: 脚 , 1: 腿膝盖 , 2: 大腿根部 , 3: 另一个大腿根部 , 4: 另一个腿膝盖 , 5: 另一个脚 ,7: 裤裆中心
            7： 脖子根， 8：下巴， 9：头顶。
            10： 手腕 ，11： 手肘 ，12： 肩膀 ，13： 肩膀 ，14： 手肘，15： 手腕
        """
        center = current_infoemation['objpos']  # 当前人体的中心点坐标。
        scale = current_infoemation['scale_provided']  # 当前图片的人体尺度

        # if center[0] != -1:
        #     center[1] = center[1] + 15*scale

        img = cv2.imread(img_path)
        cv2.imwrite('1.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        # 绘制一下没有改变尺寸之前的关键点位置。
        """  注意：关键点坐标是3纬，第三维表示当前点是否可见，第三纬=0的时候，其坐标点为 （0，0），因为其点不可见。
            [[0.0, 0.0, 0.0], [642.0, 568.0, 1.0], [731.0, 355.0, 0.0], [795.0, 379.0, 1.0], 
            [791.0, 692.0, 1.0], [0.0, 0.0, 0.0], [763.0, 367.0, 0.0], [518.0, 329.0, 0.0], 
            [495.278, 330.493, 1.0], [327.722, 341.507, 1.0], [423.0, 489.0, 1.0], [495.0, 381.0, 0.0], 
            [506.0, 249.0, 0.0], [530.0, 408.0, 1.0], [543.0, 568.0, 1.0], [433.0, 632.0, 1.0]]
        """
        # for i,k in enumerate(points):
        #     cv2.circle(img, (int(k[0]), int(k[1])), 1, (255, 0, 0), 8)
        #     cv2.putText(img, str(i), (int(k[0]+20), int(k[1]+30)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        # cv2.imwrite('orgin.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])


        # 图片的长宽。
        img_width = img.shape[0]  # x 竖轴
        img_height = img.shape[1]  # y 横轴
        # print(img_width, img_height)

        # 修改为正方形，以大的边为准，小的边补充黑色。
        pad_w, pad_h = 0, 0  # pad_w：用于竖轴扩充。 pad_h：用于横轴扩充。
        if img_width > img_height:  # x 竖轴 > y 横轴,
            pad_h = int((img_width - img_height) / 2)  # pad_h：用于左右扩充。
            img = np.pad(img, ((0, 0), (pad_h, pad_h), (0, 0)), 'constant', constant_values=0)
            # 人体中心点坐标也要跟着变换.
            center[0] += pad_h
        elif img_width < img_height:    # x 竖轴 < y 横轴, pad_w 用于上下扩充。
            pad_w = int((img_height - img_width) / 2)
            img = np.pad(img, ((pad_w, pad_w), (0, 0), (0, 0)), 'constant', constant_values=0)
            # 人体中心点坐标也要跟着变换.
            center[1] += pad_w

        # cv2.imwrite('2.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])
        # 接下来应该改变他的关键点的位置了。
        new_points = []
        # print(pad_w, pad_h)
        # print(points)
        for i, k in enumerate(points):
            x = 0.0
            y = 0.0
            v = 0.0
            if bool(k[2]) or bool(k[0]) or bool(k[1]):
                x = k[0] + pad_h
                y = k[1] + pad_w
                v = k[2]

            new_points.append([x, y, v])

        # print(new_points)
        # for i,k in enumerate(new_points):
        #     cv2.circle(img, (int(k[0]), int(k[1])), 1, (0, 255, 0), 5)
        #     cv2.putText(img, str(i), (int(k[0]+20), int(k[1]+30)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        # cv2.circle(img, (int(center[0]), int(center[1])), 1, (255, 0, 0), 10)
        # cv2.imwrite('new.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])

        # 最新的关键点位置
        kpt = []
        # 关键点对应匹配位置.  x/x尺寸*368
        for i, k in enumerate( new_points ):
            pos_0 = ( k[0] / img.shape[0] ) * self.width
            pos_1 = ( k[1] /img.shape[1] ) * self.height
            pox_2 = k[2]
            kpt.append([pos_0, pos_1, pox_2])
        # 中心点也变化
        center[0] = (center[0]/img.shape[0]) * self.width
        center[1] = (center[1]/img.shape[1]) * self.height
        # 图像转为 3368*368
        img = cv2.resize(img, (self.width, self.height))
        # print(new_points)
        # for i,k in enumerate(kpt):
        #     cv2.circle(img, (int(k[0]), int(k[1])), 1, (0, 255, 0), 5)
        #     cv2.putText(img, str(i), (int(k[0]+20), int(k[1]+30)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        # cv2.circle(img, (int(center[0]), int(center[1])), 1, (255, 0, 0), 10)
        # cv2.imwrite('new.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])

        height, width, _ = img.shape

        # 用于存储 heatmap : [368/8, 368/8, 16+1]
        heatmap_height = int(height / self.stride)
        heatmap_width = int(width / self.stride)
        heatmap = np.zeros((heatmap_height, heatmap_width, int(len(kpt) + 1)), dtype=np.float32)
        # 生成 heatmap
        for i in range(len(kpt)):
            # 关键点除以stride.
            x = int(kpt[i][0]) * 1.0 / self.stride
            y = int(kpt[i][1]) * 1.0 / self.stride

            # 输入heatmap的h,w. 关键点x,y坐标. 以及 高斯参数sigma
            heat_map = guassian_kernel(size_h=heatmap_height,size_w=heatmap_width, center_x=x, center_y=y, sigma=self.sigma)

            heat_map[heat_map > 1] = 1
            heat_map[heat_map < 0.0099] = 0
            heatmap[:, :, i + 1] = heat_map
        # 第0个位置存放的heatmap是背景.
        heatmap[:, :, 0] = 1.0 - np.max(heatmap[:, :, 1:], axis=2)  # for background

        # 中心点热度图: [46, 46, 1]
        centermap = np.zeros((heatmap_height, heatmap_width, 1), dtype=np.float32)
        center_map = guassian_kernel(size_h=heatmap_height, size_w=heatmap_width,
                                     center_x=int(center[0] / self.stride), center_y=int(center[1] / self.stride),
                                     sigma=3)
        center_map[center_map > 1] = 1
        center_map[center_map < 0.0099] = 0
        centermap[:, :, 0] = center_map

        # transformer
        img = transforms.ToTensor()(img)
        img = transforms.Normalize((128.0, 128.0, 128.0), (256.0, 256.0, 256.0))(img)
        heatmap = transforms.ToTensor()(heatmap)
        centermap = transforms.ToTensor()(centermap)


        return img, heatmap, centermap, img_path

        # for i,k in enumerate(kpt):
        #     cv2.circle(img, (int(k[0]), int(k[1])), 1, (0, 255, 0), 5)
        #     cv2.putText(img, str(i), (int(k[0]+20), int(k[1]+30)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        # cv2.imwrite('new.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])

    def __len__(self):
        return len(self.img_List)




if __name__ == '__main__':

    train_dataset = myMPIIDataset(root_dir="../datasets/MPIII/", sigma=3, is_train=True, transform=None)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=1, shuffle=True, num_workers=0)
    print(len(train_loader))

    for train_step, train_data in enumerate(train_loader):

        img, heatmap, centermap, img_path = train_data

        print(img.shape)


