from model import myUnipose
import torch
import cv2
import numpy as np
import torchvision.transforms as transforms
import torch.nn.functional as F


"""
    1, 加入旋转
    
"""

def trans(img, width, height):
    # (720, 1280, 3)

    # 图片的长宽。
    img_width = img.shape[0]  # x 竖轴
    img_height = img.shape[1]  # y 横轴
    # print(img_width, img_height)

    # 修改为正方形，以大的边为准，小的边补充黑色。
    pad_w, pad_h = 0, 0  # pad_w：用于竖轴扩充。 pad_h：用于横轴扩充。
    if img_width > img_height:  # x 竖轴 > y 横轴,
        pad_h = int((img_width - img_height) / 2)  # pad_h：用于左右扩充。
        img = np.pad(img, ((0, 0), (pad_h, pad_h), (0, 0)), 'constant', constant_values=0)

    elif img_width < img_height:  # x 竖轴 < y 横轴, pad_w 用于上下扩充。
        pad_w = int((img_height - img_width) / 2)
        img = np.pad(img, ((pad_w, pad_w), (0, 0), (0, 0)), 'constant', constant_values=0)

    img = cv2.resize(img, (width, height))
    # cv2.imwrite('trans.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])

    img_tensor = transforms.ToTensor()(img)
    img_tensor = transforms.Normalize((128.0, 128.0, 128.0), (256.0, 256.0, 256.0))(img_tensor)

    return img_tensor, img

def get_kpts(heat, img_h, img_w):

    heat = heat.clone().cpu().data.numpy()
    map_6 = heat[0]  # (17, 368, 368)

    kpts = []
    for m in map_6[1:]:  # 从第2个开始，因为第0个是背景。

        # 获取一个/组int类型的索引值在一个多维数组中的位置
        h, w = np.unravel_index(m.argmax(), m.shape)  # (x, y) 坐标

        x = int(w * img_w / m.shape[1])
        y = int(h * img_h / m.shape[0])

        kpts.append([x, y])

    return kpts


def draw_paint(img, kpts):
    """
    opencv读进来的是numpy数组，是uint8类型，0-255范围，图像形状是（H,W,C），读入的顺序是BGR
    :param img: cv2
    :param kpts:
    :return:
    """

    limbSeq = [[8, 9], [7, 12], [12, 11], [11, 10], [7, 13], [13, 14], [14, 15], [7, 6], [6, 2], [2, 1], [1, 0], [6, 3],
               [3, 4], [4, 5], [7, 8]]

    colors = [[000, 000, 255], [000, 255, 000], [000, 000, 255], [255, 255, 000], [255, 255, 000],
              [255, 000, 255],[000, 255, 000],  [255, 000, 000], [255, 255, 000], [255, 000, 255],
              [000, 255, 000], [000, 255, 000], [000, 000, 255], [255, 255, 000], [255, 000, 000]]

    # 画点
    count = 0
    for i,k in enumerate(kpts):
        cv2.circle(img, (int(k[0]), int(k[1])), 1, (0, 255, 0), 3)
        cv2.putText(img, str(i), (int(k[0]+20), int(k[1]+30)), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (0, 0, 255), 1)
        count += 1

    # 画线
    for i in range(len(limbSeq)):
        cur_im = img.copy()
        limb = limbSeq[i]
        [Y0, X0] = kpts[limb[0]]
        [Y1, X1] = kpts[limb[1]]

        if X0 != 0 and Y0 != 0 and X1 != 0 and Y1 != 0:
            if i < len(limbSeq) - 4:
                cv2.line(cur_im, (Y0, X0), (Y1, X1), colors[i], 3)
            else:
                cv2.line(cur_im, (Y0, X0), (Y1, X1), [0, 0, 255], 3)

        img = cv2.addWeighted(img, 0.2, cur_im, 0.8, 0)

    cv2.imwrite("pred.png", img)
    print("预测到的点数为 ： ",count)


model = myUnipose(num_classes=16, output_stride=16).cuda()
model.eval()
model.load_state_dict(torch.load('./weight/' + 'unipose__epoch-7_mAP-0.5433633462569847_best.pth'))

# img_path = "../datasets/MPIII/images/001756546.jpg"

img_path = "./test/frame20.png"

img = cv2.imread(img_path)  # (720, 1280, 3)
# cv2.imwrite('orgin.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])

img_tensor, img = trans(img, 368, 368)  # torch.Size([3, 368, 368]) , cv2
# cv2.imwrite('orgin.png', img, [int(cv2.IMWRITE_JPEG_QUALITY), 95])

img_tensor = torch.unsqueeze(img_tensor, 0)
heat = model(img_tensor.cuda())  # torch.Size([1, 17, 46, 46])

heat = F.interpolate(heat, size=img_tensor.size()[2:], mode='bilinear', align_corners=True)  # torch.Size([1, 17, 368, 368])

kpts = get_kpts(heat, img_h=368.0, img_w=368.0)

draw_paint(img, kpts)











