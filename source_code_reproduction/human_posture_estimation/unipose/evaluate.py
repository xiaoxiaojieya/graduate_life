from __future__ import absolute_import, division, print_function
import numpy as np


# 计算 preds, target 距离
def calc_dists(preds, target, normalize):
    """
    :param preds:  (8, 17, 2)
    :param target:   (8, 17, 2)
    :param normalize:  (8, 2)
    :return:
    """
    preds = preds.astype(np.float32)
    target = target.astype(np.float32)

    dists = np.zeros((preds.shape[1], preds.shape[0]))  # [17, batch]

    for n in range(preds.shape[0]):  # batch
        for c in range(preds.shape[1]):  # 17
            # 当 target的坐标数据都 >1 的时候才进行正则化 求距离。
            if target[n, c, 0] > 1 and target[n, c, 1] > 1:
                normed_preds = preds[n, c, :] / normalize[n]  # (x,y)
                normed_targets = target[n, c, :] / normalize[n]
                # 求二范数 其实就是距离。
                dists[c, n] = np.linalg.norm(normed_preds - normed_targets)
            else:
                dists[c, n] = -1

    # 返回 每个batch 中 pred 与 target 中 17个点 的距离 。
    return dists

# 距离的准确率： 传入的某个点的距离中，有多少点的距离小于threshold才算入准确率。
"""
    比如当前batch为8，关键点为左手，那么传进来的数据是8个左手的距离数据，当这个8个数据有4个小于threshold的时候就说明当前准确率为0.5
"""
def dist_acc(dists, threshold=0.5):
    # print(dists)  # 长度为 batch

    dist_cal = np.not_equal(dists, -1)  # 返回 真假 list
    num_dist_cal = dist_cal.sum()  # 求和 数值为 不是-1 的个数。

    # 不全为-1的时候才计算。
    if num_dist_cal > 0:
        # less(x1,x2 [,out]):检查x1是否小于x2。  只有小于阀值的才计数。
        return np.less(dists[dist_cal], threshold).sum() * 1.0 / num_dist_cal
    else:
        return -1

# 从预测矩阵中得到 每一层 最大值所在的坐标 以及 最大值.
def get_max_preds(batch_heatmaps):
    # [batch, 17, 46, 46]

    batch_size = batch_heatmaps.shape[0]  # 批次
    num_joints = batch_heatmaps.shape[1]  # 17
    width = batch_heatmaps.shape[3]  # 宽度

    # [batch, 17, 46*46]
    heatmaps_reshaped = batch_heatmaps.reshape((batch_size, num_joints, -1))
    # idx, maxvals 分别为heatmap转换为一维向量之后中的 最大值索引 与 最大值.
    idx = np.argmax(heatmaps_reshaped, 2)  # 沿着最后一维,得到最后一维中最大值的索引. [batch,17]
    maxvals = np.amax(heatmaps_reshaped, 2)  # 沿着最后一维返回最大值.  [batch,17]
    maxvals = maxvals.reshape((batch_size, num_joints, 1))  # [batch, 17, 1]
    idx = idx.reshape((batch_size, num_joints, 1))  # [batch, 17, 1]

    # 将idx沿着y轴复制2倍. 然后用最后的2维来表示 (x,y)
    preds = np.tile(idx, (1, 1, 2)).astype(np.float32)  # (batch, 17, 2)
    # 此时的坐标是 46*46 一维的,所以下边要给她转换为2为矩阵中的 (x,y) 坐标.

    # 最后一维是列向量.
    preds[:, :, 0] = (preds[:, :, 0]) % width  # 对宽度求余便可以得到x轴的坐标了(竖轴的偏移).
    preds[:, :, 1] = np.floor((preds[:, :, 1]) / width)  # 向下取整就可以得到y轴的坐标.

    """
        mask矩阵的作用是: 如果最大值位置上的数据>0,则让他可显示出来, 否则 令他为0.
        np.greater(x1, x2) : 检查x1是否大于x2, 返回bool类型的矩阵.
    """
    pred_mask = np.tile(np.greater(maxvals, 0.0), (1, 1, 2))
    pred_mask = pred_mask.astype(np.float32)

    # 预测坐标作用上mask矩阵,保证预测坐标中的数据都是非负的.
    preds *= pred_mask

    return preds, maxvals


def accuracy(output, target, hm_type='gaussian'):
    # 得到heatmap的关键点 list , 17个
    idx = list(range(output.shape[1]))  # [0, 1, ...,16]
    norm = 1.0

    if hm_type == 'gaussian':
        # pred 与 target 分别是 [batch, 17, 2] 的坐标，表示出每个关键点的坐标。
        pred, _ = get_max_preds(output)
        target, _ = get_max_preds(target)

        h = output.shape[2]  # 高 46
        w = output.shape[3]  # 宽 46
        # [batch, 2], 里边的数据都是 4.6 就是正杂化参数。
        norm = np.ones((pred.shape[0], 2)) * np.array([h, w]) / 10

    # 求pred 与 target 中 17个点 的距离， norm是求距离之前的数据归一化参数。
    # print("pred : ",pred.shape)  #  (8, 17, 2)
    # print("target : ",target.shape)  #  (8, 17, 2)
    dists = calc_dists(pred, target, norm)  # [17, batch] 每个batch中17个点的距离（预测坐标与真实坐标的距离）。

    # 准确率：长度为 17
    acc = np.zeros((len(idx)))

    avg_acc = 0
    cnt = 0  # 有多少有效点
    visible = np.zeros((len(idx)))  # 标识每个点是否可见列表 长 17
    # 求出所有批次组成的准确率。
    for i in range(len(idx)):  # 遍历每个点
        # dists[idx[i]] 是当前所有的batch中的第i个关键点的距离值。
        acc[i] = dist_acc(dists[idx[i]])  # 当前关键点i的所有batch组成准确率的计算方法。

        # 准确率存在数据，则当前点置为可见。
        if acc[i] >= 0:
            visible[i] = 1

            # 这两个是为了求avg作准备的。
            avg_acc = avg_acc + acc[i]
            cnt += 1
        else:
            acc[i] = 0

    avg_acc = avg_acc / cnt if cnt != 0 else 0

    # 第0个位置放入平均acc
    if cnt != 0:
        acc[0] = avg_acc

    return acc, cnt, pred, visible
