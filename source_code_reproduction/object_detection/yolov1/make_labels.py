import os
import xml.etree.ElementTree as ET


# 没问题。
def convert(size, box):
    """
    传入图片尺寸 size=(w,h) 与图像的四个点 box=(xmin,xmax,ymin,ymax)，进行归一化。

    返回归一化后的中心点坐标x，y以及归一化的w，h。
    （这里归一化只是针对原图，真正用的时候还要加入扩充维度之后，在归一化）
    """
    # size：[w,h] 是相片的宽和高。
    # box：(xmin，xmax，ymin，ymax) 就是边框的边界点。

    # 归一化宽高的尺度（也就是当前图片的 宽高）。
    dw = 1. / size[0]
    dh = 1. / size[1]

    # 中心点坐标
    x = (box[0] + box[1]) / 2.0  # x 横轴中心点
    y = (box[2] + box[3]) / 2.0  # y 纵轴中心点

    # 宽高(box是当前物体的边框)
    w = box[1] - box[0]  # 当前边框的宽度
    h = box[3] - box[2]  # 当前边框的高度

    # 将当前边框的x,y,w,h都归一化。
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh

    # 返回的数据是除以宽高之后归一化的数据。
    return (x, y, w, h)

# 没问题。
def convert_annotation(image_id, root_dir, CLASSES):
    """
    image_id 传入 DATASET_PATH+'Annotations' 中的每一个 xml文件。

    提取为 c,x,y,w,h （x,y是当前box的中心，w,h是box的宽高，行数就是类别数量）

    这的 c,x,y,w,h 是归一化后的，归一化是 convert 函数。
    """

    # in_file对应着Annotations中的具体的.xml文件路径。
    # in_file = open(self.DATASET_PATH + 'Annotations/%s' % (image_id))
    in_file = open(root_dir + 'Annotations/%s' % (image_id))

    # image_id就是这个xml文件的 文件名字。
    image_id = image_id.split('.')[0]

    # ET是操作xml文件，导入具体的xml文件。
    tree = ET.parse(in_file)
    # 获取第一标签
    root = tree.getroot()
    # 获取size标签。  （size存放的img尺寸）
    size = root.find('size')
    # 获取宽高
    w = int(size.find('width').text)
    h = int(size.find('height').text)

    # 获取object标签。（object标签的个数就是这个xml文件中可被识别的物体的个数）
    # 迭代出我们需要的信息

    objectNums = len(root.findall('object'))

    if objectNums > 1:

        # 输出到yoloLabels文件夹中（先在DATASET_PATH下新建一个yoloLabels文件夹）
        # out_file = open(self.DATASET_PATH + 'yoloLabels/%s.txt' % (image_id), 'w')
        out_file = open(root_dir + 'yoloLabels/%s.txt' % (image_id), 'w')

        for obj in root.iter('object'):

            # difficult标签。（1标识较难识别）
            difficult = obj.find('difficult').text
            # name标签。
            cls = obj.find('name').text

            # 如果当前object的类别不在我们定义的CLASSES中，或者 太难识别，就不处理这个object了。
            # if cls not in self.CLASSES or int(difficult) == 1:
            #     continue

            # 得到某个类别的id，是CLASSES列表中的下标。
            cls_id = CLASSES.index(cls)

            # 得到边框的标签
            xmlbox = obj.find('bndbox')

            # 得到四个坐标点 (xmin，xmax，ymin，ymax)
            points = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text),
                      float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))

            # 传入图片尺寸与 此边框 的四个点。
            # 返回归一化后的(x, y, w, h) ，x,y是当前边框中心点，w,h当前边框是宽高。
            bb = convert((w, h), points)

            # 将一个框子的 cls x y w h 写入文件。（有多个框的时候，每个框独占一行）
            out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')

# 没问题。
def make_label_txt(root_dir, CLASSES):
    """
    将 DATASET_PATH+'Annotations' 中的所有文件数据提取出来，形成labels。
    其中 Annotations中的每一个xml文件记录的是每个物体+对应的边框的xml格式。

    其中 labels中的每一个txt文件存放的是：c x y w h. （有几行就是有几个类别）
    在labels文件夹下创建image_id.txt，对应每个image_id.xml提取出来的bbox信息
    """

    # filenames是 Annotations中所有文件名字的列表。
    # filenames = os.listdir(self.DATASET_PATH + 'Annotations')
    filenames = os.listdir(root_dir + 'Annotations')

    # 传入每一个文件名，提取文件内容。
    for file in filenames:
        # 传入文件名。
        convert_annotation(file, root_dir=root_dir, CLASSES=CLASSES)


if __name__ == '__main__':

    CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
                   'diningtable',
                   'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

    train_root = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2012/"
    test_root = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2007/"

    make_label_txt(train_root, CLASSES)

    make_label_txt(test_root, CLASSES)