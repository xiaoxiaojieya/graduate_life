import os
import torch
import numpy
import numpy as np

import cv2
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import matplotlib.patches as patches


# 没问题。
def show_labels_image(imgname, root_dir, CLASSES):
    """imgname是输入的图像的名称，无下标"""
    img = cv2.imread(root_dir + 'JPEGImages/' + imgname + ".jpg")
    print(img)

    h, w = img.shape[:2]
    print(w, h)

    with open(root_dir + "yoloLabels\\" + imgname + ".txt", 'r') as flabel:
        for label in flabel:
            label = label.split(' ')
            label = [float(x.strip()) for x in label]
            # 18（类别） 0.763（x） 0.4880597014925373(y) 0.47400000000000003(w) 0.7850746268656716(h)

            # pt1 = (500, 295)
            pt1 = (int(label[1] * w - label[3] * w / 2), int(label[2] * h - label[4] * h / 2))
            # pt1 = (500, 295)
            pt2 = (int(label[1] * w + label[3] * w / 2), int(label[2] * h + label[4] * h / 2))

            cv2.putText(img, CLASSES[int(label[0])], pt1, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
            # pt1是起始点的(x,y)。 pt2是终止点的(x,y)。
            cv2.rectangle(img, pt1, pt2, (0, 0, 255, 2))

    cv2.imshow("img", img)
    cv2.waitKey(0)


def show_bbox_by_pred(imageTensor, CLASSES, pred=None):
    # print("imageTensor : ", imageTensor.shape)  # torch.Size([1, 3, 448, 448])
    # print("pred : ", pred.shape)  # torch.Size([1, 30, 7, 7])
    """
    输入 pred：torch.Size([1, 30, 7, 7])，其中每个30的数据是：[px,py,w,h,1,px,py,w,h,1,cls,cls,cls,......],
   (px,py)是相对于当前格子（未resize）的中心坐标（乘了 7 了），（w,h）是相对于未resize整个图片的。

    根据 原图 imageTensor 与 预测的prea=torch.Size([1, 30, 7, 7]) 绘制出box
    :param imageTensor:
    :param pred:
    :return:
    """

    # 加载图片
    unloader = transforms.ToPILImage()
    image = unloader(imageTensor.squeeze(0))

    image = cv2.cvtColor(numpy.asarray(image), cv2.COLOR_RGB2BGR)

    for m in range(7):  # 行

        for n in range(7):  # 列
            # print(m,n)
            # print(labels[m,n,:])

            # (起始xy)，(终止xy)
            # print("( " ,m,n,")",labels[m,n,0],labels[m,n,1],labels[m,n,2],labels[m,n,3])
            if pred[0,4,m,n].detach().numpy() > 0.09 :

                cv2.rectangle(image,
                              (int(((pred[0, 0, m, n] + m) / 7 - pred[0, 2, m, n] / 2) * 448),
                               int(((pred[0, 1, m, n] + n) / 7 - pred[0, 3, m, n] / 2) * 448)),
                              (int(((pred[0, 0, m, n] + m) / 7 + pred[0, 2, m, n] / 2) * 448),
                               int(((pred[0, 1, m, n] + n) / 7 + pred[0, 3, m, n] / 2) * 448)),
                              (0, 0, 255, 2)
                              )

                # 找出类别索引
                # print(pred.shape)
                cls_data = pred[0,10:,m,n].tolist()
                # print(cls_data)

                _id = 30
                maxdata = 0.0
                for i in range(len(cls_data)):
                    if cls_data[i] >= maxdata:
                        maxdata = cls_data[i]
                        _id = i

                # print(_id)
                if _id < 30:
                    cv2.putText(image, CLASSES[_id],
                                (int(((pred[0, 0, m, n] + m) / 7 - pred[0, 2, m, n] / 2) * 448),
                                 int(((pred[0, 1, m, n] + n) / 7 - pred[0, 3, m, n] / 2) * 448)),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))

            cv2.rectangle(image, (int(n * 64), int(m * 64)), (int((n + 1) * 64), int((m + 1) * 64)), (0, 150, 0, 1))

    cv2.imshow("OpenCV", image)
    cv2.waitKey()


def convert_bbox2labels(bbox, NUM_BBOX, CLASSES):
    """
   输入的是扩充完维度单位化后的 bbox: [c,x,y,w,h, c,x,y,w,h, c,x,y,w,h...]

   输出 labels(7, 7, 30)，其中每个30的数据是：[px,py,w,h,1,px,py,w,h,1,cls,cls,cls,......],
   (px,py)是相对于当前格子（未resize）的中心坐标（乘了 7 了），（w,h）是相对于未resize整个图片的。
   调整格式目的是loss方便计算。

   """

    # 输入bbox： [c,x,y,w,h, c,x,y,w,h, c,x,y,w,h...]
    # print("输入：",bbox)

    # 图片分为7行7列。因为传入的bbox数据是归一化的，所以宽高都是1，然后将1分成7分.
    gridsize = 1.0 / 7  # 用于将bbox数据分格子。
    # labels维度：[7,7,5*2+20]，格子7*7，每个框子5个数据，2个框子，还有20个标识类别。
    labels = np.zeros((7, 7, 5 * NUM_BBOX + len(CLASSES)))
    # labels:  (7, 7, 30)

    # self.NUM_BBOX = 2，只循环bbox中的两个格子数据即可。
    for i in range(NUM_BBOX):  #

        # 当前bbox中心x，y处于第几个网格。
        """
        这里的 gridx，gridy 是单位化的边框数据的中心位于划分 7*7 之后的哪一个cell中。
        意思就是将单位化后的边框中心分七分，看看他在哪个里边。
        gridsize = 1/7
        """
        gridx = int(bbox[i * 5 + 1] // gridsize)  # 当前bbox中心在网格第gridx列
        gridy = int(bbox[i * 5 + 2] // gridsize)
        # print("coor ： ",gridx, gridy)  # 2 3  中心点没问题

        # 中心坐标 相对于他中心点 所在的所在网格的偏移量。
        # 也就是原来的xy是针对整个图的中心点偏移，现在给他归于他所在的格子的偏移了。
        gridpx = bbox[i * 5 + 1] / gridsize - gridx  # 当前bbox中心相对位置
        gridpy = bbox[i * 5 + 2] / gridsize - gridy
        # print(gridpx, gridpy)  # 0.8770000000000002 0.7053333333333334

        # labels：7*7*（5*2+20）  , [c,x,y,w,h ，c,x,y,w,h ，c,x,y,w,h...]  ==>7*7*（5*2+3），13：[针对某个小格子的偏移，以及宽度，是否有数据，...]
        # 先使用前两维度gridy, gridx确定出这个点位于7*7网格中的那个网格。第三维度先取0:5前五个位置。
        # 即：第gridy行，gridx列的前五个数据是：相对于这个小格子的x，y偏移，w以及h，并且将是否有数据置为1.
        """
        labels[gridy, gridx, 0:5] ：指定了第 (gridy, gridx) 个格子的 前五个数据
        labels重新放入数据：[所在格子的x偏移，所在格子的y偏移，宽度，高度，预测]
        """
        # labels[gridy, gridx, 0:5] = np.array([gridpx, gridpy, bbox[i * 5 + 3], bbox[i * 5 + 4], 1])
        # # 扩充两次是为了后边损失计算的方便。
        # labels[gridy, gridx, 5:10] = np.array([gridpx, gridpy, bbox[i * 5 + 3], bbox[i * 5 + 4], 1])

        labels[gridx,gridy, 0:5] = np.array([gridpx, gridpy, bbox[i * 5 + 3], bbox[i * 5 + 4], 1])
        # 扩充两次是为了后边损失计算的方便。
        labels[gridx,gridy,5:10] = np.array([gridpx, gridpy, bbox[i * 5 + 3], bbox[i * 5 + 4], 1])

        # 设置当前 gridy*gridx*（后边的是类别标签），设置正确的类别是1，其余是0.
        labels[gridx, gridy, 10 + int(bbox[i * 5])] = 1  # class的种类标签
        # print(self.CLASSES[int(bbox[i * 5])])

    # print(labels.shape)  # (7, 7, 30)
    # print(labels[1,1,:])

    # labels(7, 7, 30)，其中每个30的数据是：[px,py,w,h,1,px,py,w,h,1,cls,cls,cls,......]
    return labels