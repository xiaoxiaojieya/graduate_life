import torch
from tools import show_bbox_by_pred
from myDatasets import MYDATA
import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

inputsize = 448
NUM_BBOX = 2
CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
           'diningtable',
           'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

train_dir = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2012/"
test_dir = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2007/"

batchsize = 1
train_data = MYDATA(train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=True, is_aug=True)
train_dataloader = torch.utils.data.DataLoader(train_data, batch_size=batchsize, shuffle=True, num_workers=0)
print(len(train_dataloader))  # 715

test_data = MYDATA(train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=False, is_aug=True)
test_dataloader = torch.utils.data.DataLoader(test_data, batch_size=batchsize, shuffle=True, num_workers=0)
print(len(test_dataloader))  # 715

for i, (inputs, labels) in enumerate(train_dataloader):

    show_bbox_by_pred(inputs, labels)

    break






