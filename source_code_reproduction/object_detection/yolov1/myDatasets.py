import cv2
import numpy
import numpy as np
import torchvision.transforms as transforms
import torch
import tools
import os


"""
MYDATA 数据加载。
"""
class MYDATA():

    # 传入参数，是否训练数据。
    def __init__(self, train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=True, is_aug=True):
        self.train_dir = train_dir
        self.test_dir = test_dir
        self.inputsize = inputsize
        self.NUM_BBOX = NUM_BBOX
        self.CLASSES = CLASSES
        self.is_aug = is_aug

        self.filenames = []  # 存储数据集的文件名称
        self.imgpath = ""  # self.imgpath图片存放的路径。
        self.labelpath = ""   # self.labelpath图片的标签存放的路径。


        # 训练的话就加载2012
        if is_train:
            # 首先列出 yoloLabels 下的所有文件名字
            for file in os.listdir(self.train_dir + "yoloLabels"):
                self.filenames.append(file.split(".")[0])

            self.imgpath = self.train_dir + "JPEGImages/"
            self.labelpath = self.train_dir + "yoloLabels/"

        else:
            for file in os.listdir(self.test_dir + "yoloLabels"):
                self.filenames.append(file.split(".")[0])

            self.imgpath = self.test_dir + "JPEGImages/"
            self.labelpath = self.test_dir + "yoloLabels/"


    # 数据集长度就是文件名的个数。
    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, item):

        # print(self.filenames[item])

        # 读入image
        img = cv2.imread(self.imgpath + self.filenames[item] + ".jpg")
        # print(img.shape)   # (375, 500, 3)
        # 得到图片的h与w
        h, w = img.shape[:2]

        # inputsize = 448
        input_size = self.inputsize
        #
        padw, padh = 0, 0

        # 将图片进行扩充维度。
        """
        如果输入图片不是一个正方形，那就把他的短边两边补上黑色，填充为正方形。
        此时的正方形不一定是448*448，所以就要使用下边的 resize 将正方形变成448*448.
        (因为resize正方形的时候不会改变图片的比例)
        tips：扩充完图片别忘记了下边边框的处理哦。
        """
        if h > w:  # 高度>宽度：需要扩充宽度
            # 宽度方向两边同时扩充 1/2 的高比宽多的数量。
            padw = int((h - w) / 2)

            # 因为img本身就是像素值，所以直接填充像素就好了。
            # img是三维：(500，375, 3)。np.pad是对数组 img 扩充的。
            # 第一维前后都扩充0个0(就是不扩充)， 第二维前后都扩充padw个0（补充宽度），第三维前后都扩充0个0（不扩充）.
            img = np.pad(img, ((0, 0), (padw, padw), (0, 0)), 'constant', constant_values=0)
        elif w > h:
            padh = int((w - h) / 2)
            img = np.pad(img, ((padh, padh), (0, 0), (0, 0)), 'constant', constant_values=0)

        # 扩充好维度之后resize一下img
        img = cv2.resize(img, (input_size, input_size))
        # img：（448，448，3）

        # 图像增广，仅做转换为张量处理
        if self.is_aug:
            aug = transforms.Compose([transforms.ToTensor()])
            img = aug(img)
        # print(img.shape)  # torch.Size([3, 448, 448])


        # 读取图片对应的bbox信息，按照1维方式存储，每五个元素表示为一个bbox的（cls，x，y，w，h）
        with open(self.labelpath + self.filenames[item] + ".txt") as f:
            # bbox每一个元素是一个框子的字符串。 '0 0.41100000000000003 0.5293333333333333 0.202 0.712'
            bbox = f.read().split('\n')

        # bbox的每一个list是框子的信息：['0', '0.41100000000000003', '0.5293333333333333', '0.202', '0.712']
        bbox = [x.split() for x in bbox]
        # bbox是所有框子的数据按顺序的list：[c,x,y,w,h,c,x,y,w,h,c,x,y,w,h...]
        bbox = [float(x) for y in bbox for x in y]

        # 因为有的labes文件为空，所以这里先看看是不是空。
        if len(bbox) % 5 != 0:
            raise ValueError("File:" + self.labelpath + self.filenames[item] + ".txt" + "bbox extraction error!")

        # print("orgin : ", bbox[:])
        # [14.0, 0.681, 0.8733333333333333, 0.186, 0.17333333333333334, 14.0, 0.035, 0.3, 0.066, 0.13599999999999998]
        # [c,x,y,w,h,c,x,y,w,h,c,x,y,w,h...]

        """
        这里就是根据上边图片的扩充方式，将框子位置进行转移。
        """
        for i in range( len(bbox) // 5 ):   # (c:0,x:1,y:2,w:3,h:4)
            # print(i)

            # 设置x与w。
            if padw != 0:  # padw是扩充的宽度。
                # (c:0, x:1 , y:2 , w:3 , h:4)
                # i * 5 + 1 ：设置的是 x.
                bbox[i * 5 + 1] = ( bbox[i * 5 + 1] * w + padw ) / h  # 得到xc/input_size，h=bbox[i*5+1]*w+2*padw
                # i * 5 + 3 : 设置的是 w.
                bbox[i * 5 + 3] = (bbox[i * 5 + 3] * w) / h

            # 设置y与h。
            elif padh != 0:
                # (c:0,x:1,y:2,w:3,h:4)
                # i * 5 + 2 : 设置的是 y.
                bbox[i * 5 + 2] = (bbox[i * 5 + 2] * h + padh) / w  # 得到xc/input_size，h=bbox[i*5+1]*w+2*padw
                # i * 5 + 5 ：设置的是 h.
                bbox[i * 5 + 4] = (bbox[i * 5 + 4] * h) / w
        # 到这一步，原图 resize了，但是边框没有resize啊？ 因为边框的数据都是最后除以了总长宽，相当于单位化了，所以不用管448了。

        # print("finish : ",bbox[:])
        # 到这一步bbox中存放的数据都是扩充维度之后又归一化的数据，格式为：[c0,x1,y2,w3,h4,c5,x6,y7,w8,h9,c,x,y,w,h...]

        """
        这里表示：填充完图片+修改完bbox没问题。  
        
        """
        # unloader = transforms.ToPILImage()
        # image = img.cpu().clone()
        # image = unloader(image)
        #
        # image = cv2.cvtColor(numpy.asarray(image), cv2.COLOR_RGB2BGR)
        #
        # # (起始xy)，(终止xy)
        # cv2.rectangle(image, ( int(bbox[1]*448-bbox[3]/2*448),int(bbox[2]*448-bbox[4]/2*448)  ),  ( int(bbox[1]*448+bbox[3]/2*448),int(bbox[2]*448+bbox[4]/2*448) ), (0, 0, 255, 2) )
        # cv2.rectangle(image, (int(bbox[6] * 448 - bbox[8] / 2 * 448), int(bbox[7] * 448 - bbox[9] / 2 * 448)),(int(bbox[6] * 448 + bbox[8] / 2 * 448), int(bbox[7] * 448 + bbox[9] / 2 * 448)), (0, 0, 255, 2))
        #
        # cv2.imshow("OpenCV", image)
        # cv2.waitKey()



        # 此时需要将 [c,x,y,w,h,c,x,y,w,h,c,x,y,w,h...] 转化为 label的形式。
        """
        传入：扩充完维度以及单位化（相对于未resize而单位化）后的[c,x,y,w,h,c,x,y,w,h,c,x,y,w,h...]。
        输出 labels(7, 7, 30)，其中每个30的数据是：[px:0,py:1,w:2,h:3,1:4,px:5,py:6,w:7,h:8,1:9,cls,cls,cls,......],
            （px,py,w,h）都是相对于未resize的数据的单位化。（px,py）都乘7了。
        """
        labels = tools.convert_bbox2labels(bbox, self.NUM_BBOX, self.CLASSES)
        # print(labels.shape)  # (7, 7, 30)

        # # 加载图片
        # unloader = transforms.ToPILImage()
        # image = img.cpu().clone()
        # image = unloader(image)
        #
        # image = cv2.cvtColor(numpy.asarray(image), cv2.COLOR_RGB2BGR)
        #
        # for m in range(7):  # 行
        #
        #     for n in range(7):  # 列
        #         # print(m,n)
        #         # print(labels[m,n,:])
        #
        #         # (起始xy)，(终止xy)
        #         # print("( " ,m,n,")",labels[m,n,0],labels[m,n,1],labels[m,n,2],labels[m,n,3])
        #         cv2.rectangle(image,
        #             ( int( ((labels[m,n,0]+m)/7 - labels[m,n,2]/2 )*448 ) , int( ((labels[m,n,1]+n)/7 - labels[m,n,3]/2 )*448 )  ),
        #             ( int( ((labels[m,n,0]+m)/7 + labels[m,n,2]/2 )*448 ) , int( ((labels[m,n,1]+n)/7 + labels[m,n,3]/2 )*448 )  ),
        #             (0, 0, 255, 2)
        #         )
        #
        #
        #
        #         # 找出类别索引
        #         cls_data = labels[m,n,10:]
        #         _id = 30
        #         for i in range(len(cls_data)):
        #             if cls_data[i] == 1.0:
        #                 # print(i)
        #                 _id = i
        #
        #         if _id < 30:
        #             cv2.putText(image, myTools().CLASSES[_id ] ,
        #                         ( int( ((labels[m,n,0]+m)/7 - labels[m,n,2]/2 )*448 ) , int( ((labels[m,n,1]+n)/7 - labels[m,n,3]/2 )*448 )  ),
        #                         cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
        #
        #         cv2.rectangle(image, ( int(n*64) , int(m*64)), (int((n+1)*64) , int((m+1)*64)), (0, 150, 0, 1))
        #
        # cv2.imshow("OpenCV", image)
        # cv2.waitKey()

        # print(labels[2,4,:])
        # (7,7,30)，7*7是划分的格子，30是： [px,py,w,h,1,px,py,w,h,1,cls,cls,cls]

        labels = transforms.ToTensor()(labels)  # torch.Size([30, 7, 7])

        # 加载图片
        # unloader = transforms.ToPILImage()
        # image = img.cpu().clone()
        # image = unloader(image)
        #
        # image = cv2.cvtColor(numpy.asarray(image), cv2.COLOR_RGB2BGR)
        #
        # for m in range(7):  # 行
        #
        #     for n in range(7):  # 列
        #         # print(m,n)
        #         # print(labels[m,n,:])
        #
        #         # (起始xy)，(终止xy)
        #         # print("( " ,m,n,")",labels[m,n,0],labels[m,n,1],labels[m,n,2],labels[m,n,3])
        #         cv2.rectangle(image,
        #                       (int(((labels[0, m, n] + m) / 7 - labels[2, m, n] / 2) * 448),
        #                        int(((labels[1, m, n] + n) / 7 - labels[3, m, n] / 2) * 448)),
        #                       (int(((labels[0, m, n] + m) / 7 + labels[2, m, n] / 2) * 448),
        #                        int(((labels[1, m, n] + n) / 7 + labels[3, m, n] / 2) * 448)),
        #                       (0, 0, 255, 2)
        #                       )
        #
        #         # 找出类别索引
        #         cls_data = labels[10:, m, n]
        #         _id = 30
        #         for i in range(len(cls_data)):
        #             if cls_data[i] == 1.0:
        #                 # print(i)
        #                 _id = i
        #
        #         if _id < 30:
        #             cv2.putText(image, myTools().CLASSES[_id],
        #                         (int(((labels[0, m, n] + m) / 7 - labels[2, m, n] / 2) * 448),
        #                          int(((labels[1, m, n] + n) / 7 - labels[3, m, n] / 2) * 448)),
        #                         cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
        #
        #         cv2.rectangle(image, (int(n * 64), int(m * 64)), (int((n + 1) * 64), int((m + 1) * 64)), (0, 150, 0, 1))
        #
        # cv2.imshow("OpenCV", image)
        # cv2.waitKey()

        # print(img.shape)  # torch.Size([3, 448, 448])
        # print(labels.shape)  # torch.Size([30, 7, 7])

        return img, labels


# 测试数据能不能加载
if __name__ == '__main__':

    inputsize = 448
    NUM_BBOX = 2
    CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
               'diningtable',
               'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

    train_dir = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2012/"
    test_dir = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2007/"

    batchsize = 1
    train_data = MYDATA(train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=True, is_aug=True)
    train_dataloader = torch.utils.data.DataLoader(train_data, batch_size=batchsize, shuffle=True, num_workers=0)
    print(len(train_dataloader))  # 715

    test_data = MYDATA(train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=False, is_aug=True)
    test_dataloader = torch.utils.data.DataLoader(test_data, batch_size=batchsize, shuffle=True, num_workers=0)
    print(len(test_dataloader))  # 715

    for i, (inputs, labels) in enumerate(train_dataloader):
        print(inputs.shape)  # torch.Size([1, 3, 448, 448])
        print(labels.shape)  # torch.Size([1, 30, 7, 7])

        break

    for i, (inputs, labels) in enumerate(test_dataloader):
        print(inputs.shape)
        print(labels.shape)

        break

