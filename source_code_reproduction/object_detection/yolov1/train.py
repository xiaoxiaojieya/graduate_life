import torch
from myDatasets import MYDATA
from model import YOLOv1_resnet
from myLoss import Loss_yolov1
from torch.utils.tensorboard import SummaryWriter
import numpy as np

from tqdm import tqdm
from colorama import Fore

train_tb_writer = SummaryWriter(log_dir="runs/myvoxel/train")
test_tb_writer = SummaryWriter(log_dir="runs/myvoxel/test")

# 训练层
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)


NUM_BBOX = 2
CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
           'diningtable',
           'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']
train_dir = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2012/"
test_dir = "/home/jie/PycharmProjects/pytorchWorkSpace/0018_base_study/datasets/VOCdevkit/VOC2007/"

# 加载数据
batchsize = 12
inputsize = 448
train_data = MYDATA(train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=True, is_aug=True)
train_dataloader = torch.utils.data.DataLoader(train_data, batch_size=batchsize, shuffle=True, num_workers=0)
print(len(train_dataloader))  # 715

test_data = MYDATA(train_dir, test_dir, inputsize, NUM_BBOX, CLASSES, is_train=False, is_aug=True)
test_dataloader = torch.utils.data.DataLoader(test_data, batch_size=batchsize, shuffle=True, num_workers=0)
print(len(test_dataloader))  # 715

# 加载模型
model = YOLOv1_resnet(NUM_BBOX, CLASSES)
model.to(device)

# 将模型写入tensorboard
init_img = torch.zeros((1, 3, 448, 448), device=device)
train_tb_writer.add_graph(model, init_img)

# 断点续进
continue_training = False
weight_path = "./weights/fcn_latest.pth"

# 损失优化器
lr = 0.0001
criterion = Loss_yolov1()
optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9, weight_decay=0.0005)

scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min', factor=0.9, patience=20)


# 保存权重
def save_weights(model, path, cur_epoch, avg_loss):
    torch.save({
        "cur_epoch": cur_epoch,
        "model_state": model.state_dict(),  # 模型参数权重
        "optimizer_state": optimizer.state_dict(),  # 优化器相关内容
        "scheduler_state": scheduler.state_dict(),  # lr_shduler 相关内容
        "avg_loss": avg_loss,  # 最佳保存分数
    }, path)


if continue_training and ( not (weight_path == "")):
    checkpoint = torch.load(weight_path, map_location=torch.device('cpu'))
    model.load_state_dict(checkpoint["model_state"])

    optimizer.load_state_dict(checkpoint["optimizer_state"])
    scheduler.load_state_dict(checkpoint["scheduler_state"])

    print("Model reload !!!")

n_epoch = 1000
best_loss = 100
for epoch in range(n_epoch):

    torch.manual_seed(np.random.randint(1000000))


    train_tbar = tqdm(train_dataloader, bar_format='{l_bar}%s{bar}%s{r_bar}' % (Fore.CYAN, Fore.MAGENTA))
    model.train()
    train_count = 0
    train_loss=0.0
    for i, (inputs, labels) in enumerate(train_tbar):

        labels=labels.float()

        # 数据进入模型。
        pred = model(inputs.to(device))

        # 计算损失：
        loss=criterion(pred,labels.to(device))

        train_loss += loss.item()
        train_count += inputs.shape[0]

        # 梯度清零
        optimizer.zero_grad()

        # 计算梯度
        loss.backward()
        # 更新参数
        optimizer.step()

        train_tbar.set_description('Epoch: %d / %d || Train loss: %.6f ' % (epoch, n_epoch, train_loss / train_count))

        # 传入训练过程中的一些参数，形成坐标可视化。
        train_tb_writer.add_scalar("loss", train_loss / train_count, epoch)  # (坐标轴名字， 数据（浮点数）， 训练步数)
        train_tb_writer.add_scalar("lr", optimizer.param_groups[0]["lr"], epoch)


    test_tbar = tqdm(test_dataloader, bar_format='{l_bar}%s{bar}%s{r_bar}' % (Fore.MAGENTA, Fore.CYAN) )
    model.eval()
    test_count = 0
    test_loss = 0.0
    for i, (inputs, labels) in enumerate(test_tbar):
        labels = labels.float()

        # 数据进入模型。
        pred = model(inputs.to(device))

        # 计算损失：
        loss = criterion(pred, labels.to(device))

        test_loss += loss.item()
        test_count += inputs.shape[0]

        # 梯度清零
        optimizer.zero_grad()

        test_tbar.set_description('Epoch: %d / %d || Train loss: %.6f ' % (epoch, n_epoch, test_loss / test_count))

        # 传入训练过程中的一些参数，形成坐标可视化。
        train_tb_writer.add_scalar("loss", test_loss / test_count, epoch)  # (坐标轴名字， 数据（浮点数）， 训练步数)
        train_tb_writer.add_scalar("lr", optimizer.param_groups[0]["lr"], epoch)


    avg_loss = test_loss / test_count

    # 最后保存一下
    path = "./weights/fcn_latest.pth"
    save_weights(model, path, epoch, avg_loss)

    # 保存模型以及其他数据，方便进行断点续进
    if (avg_loss < best_loss) or (epoch == n_epoch - 1):
        print("Saving...")
        best_loss = avg_loss
        best_path = "./weights/fcn_best_" + "epoch_" + str(epoch) + "_loss_" + str(round(best_loss, 6)) + '.pth'
        save_weights(model, best_path, epoch, best_loss)

    scheduler.step(test_loss / test_count)







