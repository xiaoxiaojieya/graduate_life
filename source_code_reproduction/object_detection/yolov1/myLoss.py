import torch.nn as nn
import torch

"""
Loss_yolov1 损失层。
"""
class Loss_yolov1(nn.Module):
    def __init__(self):
        super(Loss_yolov1, self).__init__()


    def calculate_iou(self,bbox1, bbox2):
        """
        计算 iou：IoU 计算的是 “预测的边框” 和 “真实的边框” 的交集和并集的比值。
        :param bbox1: 预测
        :param bbox2: 真实
        :return:
        """

        # 交集边框。
        intersect_bbox = [0., 0., 0., 0., ]

        # 两个box没有相交，就返回0.
        if ((bbox1[2] < bbox2[0]) or (bbox1[0] > bbox2[2]) or (bbox1[3] < bbox2[1]) or (bbox1[1] > bbox2[3])):
            return 0  # 没有相交区域，或者return 0？
        else:
            # 得到交际边框的边界四个顶点。
            intersect_bbox[0] = max(bbox1[0], bbox2[0])
            intersect_bbox[1] = max(bbox1[1], bbox2[1])
            intersect_bbox[2] = min(bbox1[2], bbox2[2])
            intersect_bbox[3] = min(bbox1[3], bbox2[3])

        # 并集 = 两个box的面积和 - 交集面积和
        area1 = (bbox1[2] - bbox1[0]) * (bbox1[3] - bbox1[1])
        area2 = (bbox2[2] - bbox2[0]) * (bbox2[3] - bbox2[1])

        # 相交的面积
        area_intersect = (intersect_bbox[2] - intersect_bbox[0]) * (intersect_bbox[3] - intersect_bbox[1])

        # 返回交并比
        return area_intersect / (area1 + area2 - area_intersect)



    def forward(self, pred, labels):
        """
        得到损失计算。
        :param pred: 预测的结果：pred = torch.Size([batchsize, 30, 7, 7])
        :param labels: 真实的结果：labels = torch.Size([batchsize, 30, 7, 7])
        :return:
        """
        # print(pred.shape)
        # print(labels.shape)

        # num_gridx = 7与num_gridy = 7 对应于后两个维度，也即图片划分的网格数.
        num_gridx, num_gridy = labels.size()[-2:]

        # 无物体预测误差。
        noobj_confi_loss = 0.

        # 坐标误差
        coor_loss = 0.

        # 有物体预测误差。
        obj_confi_loss = 0.

        # 分类误差
        class_loss = 0.

        # batchsize
        b = labels.size()[0]

        # 遍历每一个batchsize，得到每个图片的数据。
        for i in range(b):
            # m是 7 (行)*7 (列) 网格中的 行。
            for m in range(7):
                # n是 7 (行)*7 (列) 网格中的 列。
                for n in range(7):
                    # labels = torch.Size([batchsize, 30, 7, 7])
                    # 30：[px,py,w,h,1 (第一个框) ,px,py,w,h,1 (第二个框),cls,cls,cls,...] # cls取1/0
                    if labels[i, 4, m, n] == 1:  # 第一个框如果包含物体（只检查labels中的第一个框）
                        """
                        将数据（px，py，w，h）（相对网格位置）转换为（x1，y1，x2，y2）（边框实际位置）
                        先将px，py转换为cx，cy，即相对网格位置转换为标准化后实际bbox的中心位置cx，cy
                        再利用（cx-w/2,cy-h/2,cx+w/2,cy+h/2）转换为xyxy的形式，用于计算iou
                        """

                        """
                        pred = torch.Size([batchsize, 30, 7, 7])
                        其中30: [px,py,w,h,1,px,py,w,h,1,cls,cls,cls,...]
                        tips：这里的px，py是针对一个像素点而言的中心坐标，而不是相对整个图片的。
                        
                        px = pred[i, 0, m, n] + m ) 是当前图片中相对于[m，n]像素网格位置上的单位化中心点坐标。
                        px + m 是得到在整个图片而言的中心点坐标。
                        （px + m）/ 7 是将中心点坐标单位化到整个图片中。
                        pred[i, 2, m, n] / 2 就是 w的一半。
                        （px + m）/ 7 - pred[i, 2, m, n] / 2 就是得到整个图片中这个边框的左上角位置。
                        """

                        """
                            
                        
                            int(((pred[0, 0, m, n] + m) / 7 - pred[0, 2, m, n] / 2) * 448),
                            int(((pred[0, 1, m, n] + n) / 7 - pred[0, 3, m, n] / 2) * 448)),
                            int(((pred[0, 0, m, n] + m) / 7 + pred[0, 2, m, n] / 2) * 448),
                            int(((pred[0, 1, m, n] + n) / 7 + pred[0, 3, m, n] / 2) * 448)),
                        """

                        # 第一个 预测 边框 bbox1_pred_xyxy = [xmin,ymin,xmax,ymax] 在图像中的真实坐标。
                        bbox1_pred_xyxy = ( ( pred[i, 0, m, n] + m ) / num_gridx  -  pred[i, 2, m, n] / 2,
                                            ( pred[i, 1, m, n] + m ) / num_gridy  -  pred[i, 3, m, n] / 2,
                                            ( pred[i, 0, m, n] + m ) / num_gridx  +  pred[i, 2, m, n] / 2,
                                            ( pred[i, 1, m, n] + m ) / num_gridy  +  pred[i, 3, m, n] / 2)

                        # 第二个 预测 边框 bbox2_pred_xyxy = [xmin,ymin,xmax,ymax] 在图像中的真实坐标。
                        bbox2_pred_xyxy = ( (pred[i, 5, m, n] + m) / num_gridx - pred[i, 7, m, n] / 2,
                                            (pred[i, 6, m, n] + m) / num_gridy - pred[i, 8, m, n] / 2,
                                            (pred[i, 5, m, n] + m) / num_gridx + pred[i, 7, m, n] / 2,
                                            (pred[i, 6, m, n] + m) / num_gridy + pred[i, 8, m, n] / 2)

                        # 真实边框 bbox_gt_xyxy 在图像中的真实坐标。

                        bbox_gt_xyxy = ( (labels[i, 0, m, n] + m) / num_gridx - labels[i, 2, m, n] / 2,
                                         (labels[i, 1, m, n] + m) / num_gridy - labels[i, 3, m, n] / 2,
                                         (labels[i, 0, m, n] + m) / num_gridx + labels[i, 2, m, n] / 2,
                                         (labels[i, 1, m, n] + m) / num_gridy + labels[i, 3, m, n] / 2)

                        """
                        1，如果完全重合：iou=1.
                        2，如果部分重合：iou 是 (0,1) 之间
                        3，如果完全不重合：iou=0.
                        """
                        # iou函数接受的数据形式是：[xmin,ymin,xmax,ymax]
                        iou1 = self.calculate_iou(bbox1_pred_xyxy, bbox_gt_xyxy)
                        iou2 = self.calculate_iou(bbox2_pred_xyxy, bbox_gt_xyxy)

                        # 选取IOU大的那个box作为起作用的边框（iou大的效果好）。
                        if iou1 > iou2:  # iou1大，于是 第一个边框起作用。
                            """
                            坐标误差: 累计的坐标误差 + pred与labels的(x,y)的欧氏距离 +pred与labels的(w,h)的开平方距离。
                            
                            [px,py,w,h,1]
                            pred[i, 0:2, m, n] : 获取px，py值。 ==》 tensor([0.7693, 0.4300])
                            pred[i, 2:4, m, n] ：获取w，h值。 ==》 tensor([0.7693, 0.4300])
                            """
                            coor_loss = coor_loss  + 5 * ( torch.sum( (pred[i, 0:2, m, n] - labels[i, 0:2, m, n] ) ** 2)
                                        + torch.sum( (pred[i, 2:4, m, n].sqrt() - labels[i, 2:4, m, n].sqrt()) ** 2))

                            """
                            物体存在的可信度误差：累计的误差 + iou与原始labels的平方和
                            这里使用的第一个边框，所以物体存在的边框用的iou1.
                            
                            [px,py,w,h,1]
                            pred[i, 4, m, n] : 1/0 . 
                            iou1：属于 [0,1] 之间。
                            """
                            obj_confi_loss = obj_confi_loss + ( pred[i, 4, m, n] - iou1 ) ** 2

                            """
                            物体不存在的可信度误差: 累计的误差 + iou与原始labels的平方和
                            这里使用的第一个边框，所以物体不存在的边框用的iou2.
                            
                            0.5 ：目的是降低不存在边框损失所占的权重。
                            """
                            noobj_confi_loss = noobj_confi_loss + 0.5 * ( (pred[i, 9, m, n] - iou2) ** 2)

                        else:  # iou2大，于是 第二个边框起作用。
                            coor_loss = coor_loss + 5 * (torch.sum((pred[i, 5:7, m, n] - labels[i, 5:7, m, n]) ** 2)
                                                 + torch.sum((pred[i, 7:9, m, n].sqrt() - labels[i, 7:9, m, n].sqrt()) ** 2))

                            obj_confi_loss = obj_confi_loss + (pred[i, 9, m, n] - iou2) ** 2

                            noobj_confi_loss = noobj_confi_loss + 0.5 * ((pred[i, 4, m, n] - iou1) ** 2)


                        """
                        [px,py,w,h,1,px,py,w,h,1,cls,cls,cls,...]
                        分类误差：累计的分类误差 + 类别预测错误率。
                        
                        pred[i, 10:, m, n] ：得到后边20个类别数据。
                        按位乘。
                        """
                        class_loss = class_loss + torch.sum((pred[i, 10:, m, n] - labels[i, 10:, m, n]) ** 2)

                    else: # 如果第一个pred中都没数据，那就是这个labels中没有框。
                        noobj_confi_loss = noobj_confi_loss + 0.5 * torch.sum( pred[i, 4:, m, n] ** 2 ) + 0.5 * torch.sum( pred[i, 9, m, n] ** 2 )


        """
        总体损失 : 坐标误差 + 物体存在的可信度误差 + 物体不存在的可信度误差 + 分类误差
        """

        loss = coor_loss + obj_confi_loss + noobj_confi_loss + class_loss

        return loss / b