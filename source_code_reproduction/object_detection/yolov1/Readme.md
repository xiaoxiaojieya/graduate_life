
# 数据集的准备

文件夹格式如下：
datasets{ VOCdevkit { VOC2007 {Anotations} {JPEGImages} {yoloLabels} } { VOC2012 {Anotations} {JPEGImages} {yoloLabels} }

yoloLabels 文件夹是自己创建的空文件夹，然后找到makelabels.py文件，写好路径就可以运行了。

# 运行 train.py

将数据集（JPEGImages） 以及标签（yoloLabels） 的路径写好就可以了。
