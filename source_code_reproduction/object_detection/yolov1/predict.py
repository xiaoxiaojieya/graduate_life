import cv2
import torch
import os
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"
import torchvision.transforms as transforms
from PIL import Image
import matplotlib.pyplot as plt
import numpy as np
from tools import show_bbox_by_pred
from model import YOLOv1_resnet


"""
predict
"""

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)


# 加载图片  2007_001825  2011_006840
img = cv2.imread("./test/1.jpg")

# 显示一下
# cv2.imshow("img",img)
# cv2.waitKey(0)  # 等待一下

# 得到图片的h与w
h, w = img.shape[:2]

# inputsize = 448
input_size = 448

padw, padh = 0, 0

# 将图片进行扩充维度。
if h > w:  # 高度>宽度，
    padw = int((h - w) / 2)  # 看看高比宽大多少，然后两边同时扩充 1/2.
    # np.pad是对数组 img 扩充的。 第一维前后都扩充0个0， 第二维前后都扩充padw个0，第三维前后都扩充0个0.
    img = np.pad(img, ((0, 0), (padw, padw), (0, 0)), 'constant', constant_values=0)
elif w > h:
    padh = int((w - h) / 2)
    img = np.pad(img, ((padh, padh), (0, 0), (0, 0)), 'constant', constant_values=0)
# 扩充好维度之后resize一下img
img = cv2.resize(img, (input_size, input_size))
# plt.imshow(img)

# 图像增广，仅做转换为张量处理
aug = transforms.Compose([transforms.ToTensor()])
img = aug(img)   # (3,448,448)
img = torch.unsqueeze(img, dim=0)
# print(img.shape)   # torch.Size([1, 3, 448, 448])
# 保存原图，用于最后的绘制。
# orginImg = img


NUM_BBOX = 2
CLASSES = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
           'diningtable',
           'dog', 'horse', 'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

# 加载模型
model = YOLOv1_resnet(NUM_BBOX, CLASSES)
model.to(device)
model.eval()

# 得到最佳的模型的权重表路径
model_weight_path = "./weights/fcn_best_epoch_174_loss_0.284961.pth"

# 加载最佳的模型的权重表
checkpoint = torch.load(model_weight_path, map_location=torch.device('cpu'))
model.load_state_dict(checkpoint["model_state"])

pred = model(img.to(device))

# print(pred.shape)  # torch.Size([1, 30, 7, 7])
# pred ： [px,py,w,h,1 ,px,py,w,h,1 ,cls,cls,cls,......]

show_bbox_by_pred(img, CLASSES, pred.to(torch.device("cpu")))



