import torch
from model import AlexNet
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json

# 预测的图片的预处理
data_transform = transforms.Compose(
    [transforms.Resize((224, 224)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# 加载预测的图片
img = Image.open("1.jpg")
# 显示一下
plt.imshow(img)

# 把图片格式化
img = data_transform(img)
# 增加一个维度batch
img = torch.unsqueeze(img, dim=0)

# 读取类别文件
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# 建立模型
model = AlexNet(num_classes=5)
# 加载保存的权重值
model_weight_path = "./AlexNet.pth"  # 路径
model.load_state_dict(torch.load(model_weight_path))  # 加载
# 关闭dropout方法
model.eval()
# 不计算梯度
with torch.no_grad():
    # 模型结果输出，squeeze把batch维度压缩掉
    output = torch.squeeze(model(img))
    # 得到每一个类别的预测概率
    predict = torch.softmax(output, dim=0)
    # argmax(predict)获取概率最大处所对应的索引值。
    predict_cla = torch.argmax(predict).numpy()
# 打印类别名称，预测概率
print(class_indict[str(predict_cla)], predict[predict_cla].item())
plt.show()