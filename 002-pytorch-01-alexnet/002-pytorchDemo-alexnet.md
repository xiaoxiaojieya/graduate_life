## model.py

```python
import torch.nn as nn
import torch


class AlexNet(nn.Module):
    def __init__(self, num_classes=1000, init_weights=False):
        super(AlexNet, self).__init__()

        # nn.Sequential（）可以将网络结构打包，命名为features，是一个专门用来提取图象特征的结构。
        self.features = nn.Sequential(
            # Conv2d(self, in_channels, out_channels（输出深度就是卷积核的个数）, kernel_size, stride=1,padding=0,
            #                  dilation=1, groups=1,
            #                  bias=True, padding_mode='zeros')
            # 这里将卷积层的大小设置为原网络的一半，其结果差不多。
            nn.Conv2d(3, 48, kernel_size=11, stride=4, padding=2),  # input[3, 224, 224]  output[48, 55, 55]
            # 经过激活函数处理数据。inplace可以看作一种增大计算量但是降低内存使用的方法。
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),                  # output[48, 27, 27]
            nn.Conv2d(48, 128, kernel_size=5, padding=2),           # output[128, 27, 27]
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),                  # output[128, 13, 13]
            nn.Conv2d(128, 192, kernel_size=3, padding=1),          # output[192, 13, 13]
            nn.ReLU(inplace=True),
            nn.Conv2d(192, 192, kernel_size=3, padding=1),          # output[192, 13, 13]
            nn.ReLU(inplace=True),
            nn.Conv2d(192, 128, kernel_size=3, padding=1),          # output[128, 13, 13]
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),                  # output[128, 6, 6]
        )

        # 同样的方法定义全连接层，classifier是一个专门用来分类的功能。
        self.classifier = nn.Sequential(
            # Dropout进行随即失活神经元，p是随机失活的比例。
            nn.Dropout(p=0.5),
            # 最后一层池化层的输出为output[128, 6, 6]，这个全连接有2048个神经元。
            nn.Linear(128 * 6 * 6, 2048),
            nn.ReLU(inplace=True),
            nn.Dropout(p=0.5),
            nn.Linear(2048, 2048),
            nn.ReLU(inplace=True),
            # 最后一层的神经元个数为数据集类别的个数num_classes
            nn.Linear(2048, num_classes),
        )
        # 初始化权重函数：init_weights=True的时候这个函数才启用。
        if init_weights:
            # 具体的初始换内容在下边。
            self._initialize_weights()

    # 正向传播过程。
    def forward(self, x):
        # 将数据传入打包好的网络结构
        x = self.features(x)
        # 展平向量，从维度为1开始展平，因为[batch,channel,height,width],所以我们只展后三个维度的内容。
        x = torch.flatten(x, start_dim=1)
        # 将数据进行全连接层的处理
        x = self.classifier(x)
        return x

    # 权重初始化函数。
    def _initialize_weights(self):
        # 继承父类的modules()，它返回一个包含所有网络模块的迭代器。
        # 得到每一个模块 m
        for m in self.modules():
            # 如果当前模块m是卷积层，就执行下边的初始化。
            if isinstance(m, nn.Conv2d):
                # 使用kaiming_normal_方法进行初始化权重weight，
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    # 如果偏置不为空的话，就用0初始化bias
                    nn.init.constant_(m.bias, 0)
            # 如果模块m是全连接层，那么就执行下边的初始化。
            elif isinstance(m, nn.Linear):
                # 通过正态分布给weight赋值，其中mean=0，std=0.01
                nn.init.normal_(m.weight, 0, 0.01)
                #bias初始化为0
                nn.init.constant_(m.bias, 0)
```





## train.py

```python
import torch
import torch.nn as nn
from torchvision import transforms, datasets, utils
import matplotlib.pyplot as plt
import numpy as np
import torch.optim as optim
from model import AlexNet
import os
import json
import time

# 指定设备
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# 数据的转化规则定义，同时定义了训练规则与测试规则。
data_transform = {
    # RandomResizedCrop：随即裁剪为224*224的大小。
    # RandomHorizontalFlip：在水平方向随机翻转。
    "train": transforms.Compose([transforms.RandomResizedCrop(224),
                                 transforms.RandomHorizontalFlip(),
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]),
    # Resize：简单的把大小裁剪为224*224
    "val": transforms.Compose([transforms.Resize((224, 224)),  # cannot 224, must (224, 224)
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])}

# 获取数据集所在的根目录。
# os.getcwd()：得到当前文件的根目录。
# os.path.join(a,b)：将a与b两个目录路径进行拼接。
# data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))  # get data root path
data_root = os.path.abspath(os.getcwd())  # get data root path
image_path = data_root + "/data_set/flower_data/"  # flower data set path
# 加载数据集
train_dataset = datasets.ImageFolder(root=image_path + "/train",
                                     transform=data_transform["train"])
# 看一下训练集有多少图片。
train_num = len(train_dataset)

# 得到train_dataset的字典。
# {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
flower_list = train_dataset.class_to_idx
# 将数据集字典的key与value反过来。
# {0: 'daisy', 1: 'dandelion', 2: 'roses', 3: 'sunflowers', 4: 'tulips'}
cla_dict = dict((val, key) for key, val in flower_list.items())
# 将我们的字典转化为为json文件。indent控制缩进
json_str = json.dumps(cla_dict, indent=4)
# 写入json文件
with open('class_indices.json', 'w') as json_file:
    json_file.write(json_str)


batch_size = 32
# 加载训练集
train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size=batch_size, shuffle=True,
                                           num_workers=0)

# 导入验证集。
validate_dataset = datasets.ImageFolder(root=image_path + "/val",
                                        transform=data_transform["val"])
val_num = len(validate_dataset)
# 加载验证集。
validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                              batch_size=batch_size, shuffle=True,
                                              num_workers=0)

# test_data_iter = iter(validate_loader)
# test_image, test_label = test_data_iter.next()
#
# def imshow(img):
#     img = img / 2 + 0.5  # unnormalize
#     npimg = img.numpy()
#     plt.imshow(np.transpose(npimg, (1, 2, 0)))
#     plt.show()
#
# print(' '.join('%5s' % cla_dict[test_label[j].item()] for j in range(4)))
# imshow(utils.make_grid(test_image))



# 得到网络，num_classes是分类数量（也是全连接最后一层的输出个数）
net = AlexNet(num_classes=5, init_weights=True)
# 使用设备
net.to(device)
# 损失函数的定义
loss_function = nn.CrossEntropyLoss()
# pata = list(net.parameters())  # 查看模型的参数
# 优化器的定义：优化对象是所有可训练的参数，学习率为0.0002
optimizer = optim.Adam(net.parameters(), lr=0.0002)
# 训练结果的保存路径
save_path = './AlexNet.pth'
# 最佳准确度
best_acc = 0.0

# 分为10个批次进行
for epoch in range(10):
    # 可以启用Dropout方法
    net.train()
    running_loss = 0.0
    # 统计一下时间而已
    t1 = time.perf_counter()

    # 遍历数据集。
    for step, data in enumerate(train_loader, start=0):
        # 将数据集分为图象，标签
        images, labels = data
        # 清空上一次的梯度信息
        optimizer.zero_grad()
        # 将图像进行（指定设备）正向传播
        outputs = net(images.to(device))
        # 计算损失
        loss = loss_function(outputs, labels.to(device))
        # 反向传播，计算当前梯度。
        # 根据当前的损失函数对每一个节点进行求偏导，从而得到使loss最小的下降速度最快的方向。
        loss.backward()
        # 根据梯度更新网络参数。如：w（new）=w（old）-lr*grad
        optimizer.step()

        # 当前批次的损失和
        running_loss += loss.item()
        # 打印训练的进度，len(train_loader)是总的步数，然后再用当前step去除可得到训练进度。
        rate = (step + 1) / len(train_loader)
        # 这里的*与.其实就是展示进度条而已。
        a = "*" * int(rate * 50)  #
        b = "." * int((1 - rate) * 50)
        print("\rtrain loss: {:^3.0f}%[{}->{}]{:.3f}".format(int(rate * 100), a, b, loss), end="")
    print()
    print(time.perf_counter()-t1)

    # 训练完一轮之后，使用验证集进行验证
    # 关闭Dropout方法
    net.eval()
    acc = 0.0  # accumulate accurate number / epoch
    # 验证的时候不计算梯度
    with torch.no_grad():
        for val_data in validate_loader:
            val_images, val_labels = val_data
            # 得到结果
            outputs = net(val_images.to(device))
            # 得到结果中的最大概率的值，作为预测。
            predict_y = torch.max(outputs, dim=1)[1]
            # 如果预测类别predict_y与标签的值val_labels对应，则就是预测成功，acc+1。
            acc += (predict_y == val_labels.to(device)).sum().item()
        # 最后看看当前验证集中有多少模型预测正确，再除以总数，可以得到准确率。
        val_accurate = acc / val_num
        # 求得最佳的模型
        if val_accurate > best_acc:
            best_acc = val_accurate
            # 保存最优的权重信息。
            torch.save(net.state_dict(), save_path)
        print('[epoch %d] train_loss: %.3f  test_accuracy: %.3f' %
              (epoch + 1, running_loss / step, val_accurate))

print('Finished Training')
```



## predict.py

```python
import torch
from model import AlexNet
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json

# 预测的图片的预处理
data_transform = transforms.Compose(
    [transforms.Resize((224, 224)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# 加载预测的图片
img = Image.open("1.jpg")
# 显示一下
plt.imshow(img)

# 把图片格式化
img = data_transform(img)
# 增加一个维度batch
img = torch.unsqueeze(img, dim=0)

# 读取类别文件
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# 建立模型
model = AlexNet(num_classes=5)
# 加载保存的权重值
model_weight_path = "./AlexNet.pth"  # 路径
model.load_state_dict(torch.load(model_weight_path))  # 加载
# 关闭dropout方法
model.eval()
# 不计算梯度
with torch.no_grad():
    # 模型结果输出，squeeze把batch维度压缩掉
    output = torch.squeeze(model(img))
    # 得到每一个类别的预测概率
    predict = torch.softmax(output, dim=0)
    # argmax(predict)获取概率最大处所对应的索引值。
    predict_cla = torch.argmax(predict).numpy()
# 打印类别名称，预测概率
print(class_indict[str(predict_cla)], predict[predict_cla].item())
plt.show()
```

​       