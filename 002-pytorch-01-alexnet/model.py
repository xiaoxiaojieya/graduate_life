import torch.nn as nn
import torch


class AlexNet(nn.Module):
    def __init__(self, num_classes=1000, init_weights=False):
        super(AlexNet, self).__init__()

        # nn.Sequential（）可以将网络结构打包，命名为features，是一个专门用来提取图象特征的结构。
        self.features = nn.Sequential(
            # Conv2d(self, in_channels, out_channels（输出深度就是卷积核的个数）, kernel_size, stride=1,padding=0,
            #                  dilation=1, groups=1,
            #                  bias=True, padding_mode='zeros')
            # 这里将卷积层的大小设置为原网络的一半，其结果差不多。
            nn.Conv2d(3, 48, kernel_size=11, stride=4, padding=2),  # input[3, 224, 224]  output[48, 55, 55]
            # 经过激活函数处理数据。inplace可以看作一种增大计算量但是降低内存使用的方法。
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),                  # output[48, 27, 27]
            nn.Conv2d(48, 128, kernel_size=5, padding=2),           # output[128, 27, 27]
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),                  # output[128, 13, 13]
            nn.Conv2d(128, 192, kernel_size=3, padding=1),          # output[192, 13, 13]
            nn.ReLU(inplace=True),
            nn.Conv2d(192, 192, kernel_size=3, padding=1),          # output[192, 13, 13]
            nn.ReLU(inplace=True),
            nn.Conv2d(192, 128, kernel_size=3, padding=1),          # output[128, 13, 13]
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),                  # output[128, 6, 6]
        )

        # 同样的方法定义全连接层，classifier是一个专门用来分类的功能。
        self.classifier = nn.Sequential(
            # Dropout进行随即失活神经元，p是随机失活的比例。
            nn.Dropout(p=0.5),
            # 最后一层池化层的输出为output[128, 6, 6]，这个全连接有2048个神经元。
            nn.Linear(128 * 6 * 6, 2048),
            nn.ReLU(inplace=True),
            nn.Dropout(p=0.5),
            nn.Linear(2048, 2048),
            nn.ReLU(inplace=True),
            # 最后一层的神经元个数为数据集类别的个数num_classes
            nn.Linear(2048, num_classes),
        )
        # 初始化权重函数：init_weights=True的时候这个函数才启用。
        if init_weights:
            # 具体的初始换内容在下边。
            self._initialize_weights()

    # 正向传播过程。
    def forward(self, x):
        # 将数据传入打包好的网络结构
        x = self.features(x)
        # 展平向量，从维度为1开始展平，因为[batch,channel,height,width],所以我们只展后三个维度的内容。
        x = torch.flatten(x, start_dim=1)
        # 将数据进行全连接层的处理
        x = self.classifier(x)
        return x

    # 权重初始化函数。
    def _initialize_weights(self):
        # 继承父类的modules()，它返回一个包含所有网络模块的迭代器。
        # 得到每一个模块 m
        for m in self.modules():
            # 如果当前模块m是卷积层，就执行下边的初始化。
            if isinstance(m, nn.Conv2d):
                # 使用kaiming_normal_方法进行初始化权重weight，
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    # 如果偏置不为空的话，就用0初始化bias
                    nn.init.constant_(m.bias, 0)
            # 如果模块m是全连接层，那么就执行下边的初始化。
            elif isinstance(m, nn.Linear):
                # 通过正态分布给weight赋值，其中mean=0，std=0.01
                nn.init.normal_(m.weight, 0, 0.01)
                #bias初始化为0
                nn.init.constant_(m.bias, 0)