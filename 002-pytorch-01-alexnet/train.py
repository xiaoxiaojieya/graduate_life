import torch
import torch.nn as nn
from torchvision import transforms, datasets, utils
import matplotlib.pyplot as plt
import numpy as np
import torch.optim as optim
from model import AlexNet
import os
import json
import time

# 指定设备
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# 数据的转化规则定义，同时定义了训练规则与测试规则。
data_transform = {
    # RandomResizedCrop：随即裁剪为224*224的大小。
    # RandomHorizontalFlip：在水平方向随机翻转。
    "train": transforms.Compose([transforms.RandomResizedCrop(224),
                                 transforms.RandomHorizontalFlip(),
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]),
    # Resize：简单的把大小裁剪为224*224
    "val": transforms.Compose([transforms.Resize((224, 224)),  # cannot 224, must (224, 224)
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])}

# 获取数据集所在的根目录。
# os.getcwd()：得到当前文件的根目录。
# os.path.join(a,b)：将a与b两个目录路径进行拼接。
# data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))  # get data root path
data_root = os.path.abspath(os.getcwd())  # get data root path
image_path = data_root + "/data_set/flower_data/"  # flower data set path
# 加载数据集
train_dataset = datasets.ImageFolder(root=image_path + "/train",
                                     transform=data_transform["train"])
# 看一下训练集有多少图片。
train_num = len(train_dataset)

# 得到train_dataset的字典。
# {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
flower_list = train_dataset.class_to_idx
# 将数据集字典的key与value反过来。
# {0: 'daisy', 1: 'dandelion', 2: 'roses', 3: 'sunflowers', 4: 'tulips'}
cla_dict = dict((val, key) for key, val in flower_list.items())
# 将我们的字典转化为为json文件。indent控制缩进
json_str = json.dumps(cla_dict, indent=4)
# 写入json文件
with open('class_indices.json', 'w') as json_file:
    json_file.write(json_str)


batch_size = 32
# 加载训练集
train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size=batch_size, shuffle=True,
                                           num_workers=0)

# 导入验证集。
validate_dataset = datasets.ImageFolder(root=image_path + "/val",
                                        transform=data_transform["val"])
val_num = len(validate_dataset)
# 加载验证集。
validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                              batch_size=batch_size, shuffle=True,
                                              num_workers=0)

# test_data_iter = iter(validate_loader)
# test_image, test_label = test_data_iter.next()
#
# def imshow(img):
#     img = img / 2 + 0.5  # unnormalize
#     npimg = img.numpy()
#     plt.imshow(np.transpose(npimg, (1, 2, 0)))
#     plt.show()
#
# print(' '.join('%5s' % cla_dict[test_label[j].item()] for j in range(4)))
# imshow(utils.make_grid(test_image))



# 得到网络，num_classes是分类数量（也是全连接最后一层的输出个数）
net = AlexNet(num_classes=5, init_weights=True)
# 使用设备
net.to(device)
# 损失函数的定义
loss_function = nn.CrossEntropyLoss()
# pata = list(net.parameters())  # 查看模型的参数
# 优化器的定义：优化对象是所有可训练的参数，学习率为0.0002
optimizer = optim.Adam(net.parameters(), lr=0.0002)
# 训练结果的保存路径
save_path = './AlexNet.pth'
# 最佳准确度
best_acc = 0.0

# 分为10个批次进行
for epoch in range(10):
    # 可以启用Dropout方法
    net.train()
    running_loss = 0.0
    # 统计一下时间而已
    t1 = time.perf_counter()

    # 遍历数据集。
    for step, data in enumerate(train_loader, start=0):
        # 将数据集分为图象，标签
        images, labels = data
        # 清空上一次的梯度信息
        optimizer.zero_grad()
        # 将图像进行（指定设备）正向传播
        outputs = net(images.to(device))
        # 计算损失
        loss = loss_function(outputs, labels.to(device))
        # 反向传播，计算当前梯度。
        # 根据当前的损失函数对每一个节点进行求偏导，从而得到使loss最小的下降速度最快的方向。
        loss.backward()
        # 根据梯度更新网络参数。如：w（new）=w（old）-lr*grad
        optimizer.step()

        # 当前批次的损失和
        running_loss += loss.item()
        # 打印训练的进度，len(train_loader)是总的步数，然后再用当前step去除可得到训练进度。
        rate = (step + 1) / len(train_loader)
        # 这里的*与.其实就是展示进度条而已。
        a = "*" * int(rate * 50)  #
        b = "." * int((1 - rate) * 50)
        print("\rtrain loss: {:^3.0f}%[{}->{}]{:.3f}".format(int(rate * 100), a, b, loss), end="")
    print()
    print(time.perf_counter()-t1)

    # 训练完一轮之后，使用验证集进行验证
    # 关闭Dropout方法
    net.eval()
    acc = 0.0  # accumulate accurate number / epoch
    # 验证的时候不计算梯度
    with torch.no_grad():
        for val_data in validate_loader:
            val_images, val_labels = val_data
            # 得到结果
            outputs = net(val_images.to(device))
            # 得到结果中的最大概率的值，作为预测。
            predict_y = torch.max(outputs, dim=1)[1]
            # 如果预测类别predict_y与标签的值val_labels对应，则就是预测成功，acc+1。
            acc += (predict_y == val_labels.to(device)).sum().item()
        # 最后看看当前验证集中有多少模型预测正确，再除以总数，可以得到准确率。
        val_accurate = acc / val_num
        # 求得最佳的模型
        if val_accurate > best_acc:
            best_acc = val_accurate
            # 保存最优的权重信息。
            torch.save(net.state_dict(), save_path)
        print('[epoch %d] train_loss: %.3f  test_accuracy: %.3f' %
              (epoch + 1, running_loss / step, val_accurate))

print('Finished Training')