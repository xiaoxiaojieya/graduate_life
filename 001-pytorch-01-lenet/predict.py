import torch
import torchvision.transforms as transforms
from PIL import Image
from model import LeNet

# 对我们输入的图片进行标准化的功能进行封装。
transform = transforms.Compose(
    [transforms.Resize((32, 32)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

net = LeNet()
# 加载训练后的模型脚本进行预测
net.load_state_dict(torch.load('Lenet.pth'))

# 导入图片
im = Image.open('1.jpg')
# 格式化图片
im = transform(im)  # 得到的格式 [C, H, W]
# 由于我们的网络是四个维度，得到的格式 [C, H, W]少一个batch
im = torch.unsqueeze(im, dim=0)  # 增加维度 [B, C, H, W]

# 不需要计算损失梯度
with torch.no_grad():
    # 传入图片
    outputs = net(im)
    # 得到最大概率的类别index
    # predict = torch.max(outputs, dim=1)[1].data.numpy()

    # 使用softmax得到概率
    predict = torch.softmax(outputs,dim=1)
# 输出预测类别
# print(classes[int(predict)])
# 输出概率
print(predict)