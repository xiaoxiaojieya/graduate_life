import torch
import torchvision
import torch.nn as nn
from model import LeNet
import torch.optim as optim
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np

# transforms.Compose将我们的预处理方法打包成一个整体，这里使用了两个预处理方法
# ToTensor：Convert a ``PIL Image`` or ``numpy.ndarray`` to tensor.
# Normalize：Normalize a tensor image with mean（均值） and standard deviation（标准差）.
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# 50000张训练图片
# root='./data'下载到的位置，download=True下载数据（第一次设置为True进行下载，以后记得改为false不让再次下载了），
# train=True导入训练集，transform=transform这是对图像预处理的函数。
train_set = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=False, transform=transform)
# torch官方给我们准备了很多的数据集，我们使用torchvision.datasets.提示的大写字母就是数据集

# 将数据集进行批次划分，batch_size=36每个批次36张图片，shuffle=True每个batch的图片都是随机提取出来的，num_workers=0载入数据的线程数win必须为0
train_loader = torch.utils.data.DataLoader(train_set, batch_size=36,
                                          shuffle=True, num_workers=0)


# 按照同样的方法导入测试集，
# 10000张验证图片
testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=False, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=10000,
                                         shuffle=False, num_workers=0)
# 将获取测试集的方法变成一个迭代器，我们每次next就可以得到一批又一批的数据
test_data_iter = iter(testloader)
test_image, test_label = test_data_iter.next()

# 类别标签，他是元组类型（值不可以改变的），index0就是plane
classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

# # 显示一下图片：显示测试的时候记得把10000改成5，不然太大了看不了
# def imshow(img):
#     # unnormalize,将图片反标准化。
#     # 刚才我们标准化的时候: out=（input-0.5）/0.5
#     # 所以现在反标准化：input=out*0.5+0.5=out/2+0.5
#     img = img / 2 + 0.5
#     # 再把tensor格式的图片还原为numpy格式
#     npimg = img.numpy()
#     plt.imshow(np.transpose(npimg, (1, 2, 0)))
#     plt.show()
#
# # print labels：打印标签
# print(' '.join('%5s' % classes[test_label[j]] for j in range(5)))
# # show images：显示图片
# imshow(torchvision.utils.make_grid(test_image))


# 导入网络
net = LeNet()
# 定义损失函数：这个损失函数已经包含了Softmax，所以我们最终的全连接层无需在定义Softmax
# This criterion combines :func:`nn.LogSoftmax` and :func:`nn.NLLLoss` in one single class.
loss_function = nn.CrossEntropyLoss()
# 定义优化器Adam：net.parameters()将net中所有可训练的参数都进行训练，lr=0.001学习率
optimizer = optim.Adam(net.parameters(), lr=0.001)

# 训练过程：总共10000张，分5批进行训练。epoch为批次。
for epoch in range(5):  # 将训练集迭代五次

    running_loss = 0.0  # 累加训练过程中的损失
    # 用循环来遍历训练集的样本，data当前训练集的数据，step训练的步数，从start=0开始
    for step, data in enumerate(train_loader, start=0):
        # 得到当前图像以及其标签
        inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        # 将图片输入网络
        outputs = net(inputs)
        # 计算损失，outputs网络预测值，labels真实值
        loss = loss_function(outputs, labels)
        # 反向传播
        loss.backward()
        # 优化器的参数更新
        optimizer.step()

        # print statistics
        # 损失累加
        running_loss += loss.item()
        # 每隔500步打印一次信息。（这里是针对当前批次里边的所有图片（500张）进行输出一下结果）
        if step % 500 == 499:
            # torch.no_grad()包裹起来的内容不需要再计算梯度，因为这里的内容只是做一个结果展示，并且这里的内容在前边都已经计算过了。
            with torch.no_grad():
                outputs = net(test_image)  # [batch, 10]
                # dim=1对输出的outputs的维度1进行寻找值，因为维度0是batch的值，[1]得到最大的标签类别。
                predict_y = torch.max(outputs, dim=1)[1]
                # 所有预测值与真实值标签对比，在求和（tensor类型）。item()再转化为数值。再除以测试样本的数目得到准确率
                accuracy = (predict_y == test_label).sum().item() / test_label.size(0)
                # epoch训练迭代的第几轮，step某一轮的多少步，running_loss是每500步的累计误差，accuracy准确率。
                print('[%d, %5d] train_loss: %.3f  test_accuracy: %.3f' %
                      (epoch + 1, step + 1, running_loss / 500, accuracy))
                # 清零累计误差
                running_loss = 0.0

print('Finished Training')
# 保存训练参数与结果
save_path = './Lenet.pth'
torch.save(net.state_dict(), save_path)