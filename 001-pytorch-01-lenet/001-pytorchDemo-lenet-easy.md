## model.py

```python
import torch.nn as nn
import torch.nn.functional as F

# 数据集说明：CIFAR10，图片都是RGB三色，每一个图片32*32的。
# lenet的数据（batch，channel，height，weight）
# 卷积层输出矩阵的维度 N=（W-F+2*P）/s+1，W输入矩阵维度，F卷积层维度，P补充的边距，s步长。

# lenet必须继承自nn.Module父类
class LeNet(nn.Module):
    # 初始化函数，进行网络的构建
    def __init__(self):
        super(LeNet, self).__init__()  # 多继承必须有父类的初始化

        # 定义第一个卷积层：（输入的特征矩阵的深度，卷积核的个数，卷积核的大小）步长默认为1。
        # 输入的特征矩阵的深度：图片有R,G,B三个通道，于是图片的深度为3，（卷积核的深度与输入数据的深度相同，因为要针对每一个深度矩阵进行卷积）。
        # 卷积核的个数：lenet的第一个卷积核为16个。（用几个卷积核，输出的矩阵的深度就是卷积核的个数）
        # 卷积核的大小：就是卷积核矩阵的维度。
        self.conv1 = nn.Conv2d(3, 16, 5)
        # 定义第一个池化层：池化层的矩阵维度为2，步距为2（不写时默认为池化矩阵的维度）。
        # 池化层的维度为2，因此会把第一个卷积层的输出矩阵维度减半。
        self.pool1 = nn.MaxPool2d(2, 2)
		
        # 第二个卷积层：上一层求出的深度为16，定义这一层卷积核32个，卷积核5*5。
        self.conv2 = nn.Conv2d(16, 32, 5)
        # 第二个池化层：又是2*2的池化矩阵。
        self.pool2 = nn.MaxPool2d(2, 2)

        # 定义三个全连接层：全连接层的输入都是一维向量。因此需要把上一步输出的[b,32,5,5]（深度为32的5*5的矩阵也就是：32个5*5的矩阵）转化为一维向量。
        # 输入32*5*5个一维数据，让全连接层中间结点为120个。因此输出数据就是120个一维数据。
        self.fc1 = nn.Linear(32*5*5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10) # 最后一层经过多少神经元，要看数据集的类型个数。

    # 向前传播函数，进行传播网络的连接。
    def forward(self, x):
        # 输出的深度为卷积核的个数。
        x = F.relu(self.conv1(x))  # input[b,3,32,32],output[b,16,28,28]
        # 经过2*2的池化层之后，原始28*28矩阵维度减半为14*14
        x = self.pool1(x) # input[b,16,28,28],output[b,16,14,14]
        
        x = F.relu(self.conv2(x)) # input[b,16,14,14],output[b,32,10,10]
        x = self.pool2(x)  # input[b,32,10,10],output[b,32,5,5]
        
        # 这个函数的功能就是将输出的矩阵[b,32,5,5]转化为一维矩阵，用以支持全连接层。-1是让第一个参数深度自适应。
        x = x.view(-1, 32*5*5)  #得到的是深度为32，800的一维向量，output（32，800）
        # 第一层输入800，经过120个神经元之后输出的是深度32，120的一维向量
        x = F.relu(self.fc1(x))  # output（32，120）
        # 第二层输入120，经过84个神经元之后输出的是深度32，84的一维向量
        x = F.relu(self.fc2(x))  # output（32，84）
        # 第三层输入84，经过10个神经元之后输出的是深度32，10的一维向量
        x = self.fc3(x)  # output（32，10）
        return x
# 测试当前网络的连接性。
import torch
input1 = torch.rand([32,3,32,32])
model = LeNet()
print(model)
output = model(input1)
```





## train.py

```python
import torch
import torchvision
import torch.nn as nn
from model import LeNet
import torch.optim as optim
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np

# 使用transforms.Compose将我们的数据预处理方法进行打包，用于对数据集的预处理。
# ToTensor()：将PIL Image或者numpy.ndarray转化为tensor（格式为(C,H,W)，并且取值在[0.0, 1.0]内）。
# Normalize((均值序列),(标准差序列))：使用均值与标准差来标准化数据（序列是针对每一个channel的值，即有多少channel序列就有多少值）。
transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# 得到训练集（50000个数据）：下载的数据放在root下边，train=True的时候数据来自训练集，否则来自测试集。
# download=True的时候下载次数据集（一般只在第一次设置为True，其他的时候都设置False）。
# transform=transform使用transform进行数据的标准化。
train_set = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=False, transform=transform)
# 加载训练集数据：加载train_set的数据，batch_size是一次训练所选取的样本数。shuffle=True的意思就是每一批的样本随机选取。
# num_workers=0载入数据的线程数，win必须为0。
train_loader = torch.utils.data.DataLoader(train_set, batch_size=36,
                                          shuffle=True, num_workers=0)

# 同样获取数据的方式得到测试集（10000个数据）：这里的train=False，因为要得到测试集数据而不是训练集数据。
testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=False, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=10000,
                                         shuffle=False, num_workers=0)

# iter(testloader)将数据加载函数形成一个迭代器，每次调用就会加载一次数据。
test_data_iter = iter(testloader)
# 得到数据的内容，所属标签
test_image, test_label = test_data_iter.next()
# 数据集的类别标签（用于将预测值与真实值对比）
classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

# 导入网络
net = LeNet()
# 导入损失函数
loss_function = nn.CrossEntropyLoss()
# 得到Adam优化器（net.parameters()将net中所有可训练的参数都进行训练），lr=0.001学习率。
optimizer = optim.Adam(net.parameters(), lr=0.001)
# 将训练集进行5批处理，epoch为批次。
for epoch in range(5):
    running_loss = 0.0
    # step为当前批次的迭代步数。
    for step, data in enumerate(train_loader, start=0):
        # 得到当前图像数据，以及其标签
        inputs, labels = data
        
        # 首先将此批次数据的梯度置0.
        optimizer.zero_grad()
        # 将图像数据输入到net中，得到输出。这里的网络训练用的 训练集train_loader。
        outputs = net(inputs)
        # 根据计算得到的输出，与本身的标签进行对比，得到损失。
        loss = loss_function(outputs, labels)
		# 向后传播
        loss.backward()
		# 执行单个步骤的优化
        optimizer.step()
        # 累加当前批次的损失值。
        running_loss += loss.item()

        # 每次都是500的倍数的时候打印一下当前训练的损失以及准确度。
        if step % 500 == 499:
            # with torch.no_grad()的目的是打印的这一步不计算梯度，让输出快一点
            with torch.no_grad():
                # 这里的网络用的 测试集test_image
                outputs = net(test_image)
				# 求输出的outputs中预测值最大的下标值。dim=1是因为outputs[0]不是数据。
                predict_y = torch.max(outputs, dim=1)[1]
				# 精确值是统计预测正确的个数，通过item将tensor数值转化为普通数值。再除以数值的总个数，得到精确值。
                accuracy = (predict_y == test_label).sum().item() / test_label.size(0)
				# 累计的损失/累积的次数 得到损失值。
                print('[%d, %5d] train_loss: %.3f  test_accuracy: %.3f' %
                      (epoch + 1, step + 1, running_loss / 500, accuracy))
				# 清理损失
                running_loss = 0.0

print('Finished Training')
# 保存训练结果
save_path = './Lenet.pth'
torch.save(net.state_dict(), save_path)
```



## predict.py

```python
import torch
import torchvision.transforms as transforms
from PIL import Image
from model import LeNet

# 得到图象预处理的内容，Resize(32,32)是把输入的图像转化为32*32的维度（因为我们的训练数据集的图片就是32*32的）。
transform = transforms.Compose(
    [transforms.Resize((32, 32)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

net = LeNet()
# 加载刚才训练生成的模型数据。
net.load_state_dict(torch.load('Lenet.pth'))

im = Image.open('1.jpg')  # 导入图片
im = transform(im)  # 使用标准化进行处理。
# 由于我们的网络是四个维度，得到的格式 [C, H, W]少一个batch
    im = torch.unsqueeze(im, dim=0)  # 在拓扑im数据上增加维度 [B, C, H, W]

with torch.no_grad():  # 不训练梯度
    outputs = net(im)
    
    # 得到最大概率的类别的index，通过numpy()将tensor数据转化为数值。
    # 比如：torch.max(outputs, dim=1)[1]得到tensor([8])。再加上numpy()变成[8]
    # predict = torch.max(outputs, dim=1)[1].numpy()

    # 使用softmax得到概率
    predict = torch.softmax(outputs,dim=1)
    
# 输出预测类别
# print(classes[int(predict)])

# 输出概率
print(predict)
```

​       