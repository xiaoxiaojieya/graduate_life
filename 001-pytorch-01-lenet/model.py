import torch.nn as nn
import torch.nn.functional as F


# 必须继承 nn.Module。
# 定义两个函数：1，初始化函数。2，前向传播函数。
class LeNet(nn.Module):

    # 初始化函数：进行一些初始化
    def __init__(self):
        # 多继承的时候必须定义一个super。
        super(LeNet, self).__init__()

        # 第一个卷积层的定义：通过nn.Conv2d(数据深度，卷积核个数，卷积核维度)来定义卷积层。进入Conv2d()的初始化函数。
        # def __init__(self, in_channels（卷积层的深度与输入的特征深度相同，比如图片由RGB组成，因此它输入深度为3）,
        #                  out_channels（卷积核的个数，用几个卷积核，就会生成1个深度为多少维的特征矩阵）,
        #                  kernel_size（卷积核的大小）, stride=1（步长）,
        #                  padding=0（四周补0处理）,
        #                  dilation=1, groups=1, 这两个目前用不到
        #                  bias=True（默认使用偏置）, padding_mode='zeros')
        self.conv1 = nn.Conv2d(3, 16, 5)  #这里输入数据深度为3，使用16个卷积核，每个卷积核维度为5

        # 第一个池化层的定义：
        # 点进去发现没有初始化函数，他是继承法父类的，于是进去它的父类
        # def __init__(self, kernel_size, stride=None, padding=0, dilation=1,
        #              return_indices=False, ceil_mode=False):
        # MaxPool2d(2（池化核为2*2的）, 2（步长,若不指定则与池化核相同）)
        self.pool1 = nn.MaxPool2d(2, 2)

        # 第二个卷积层：输入深度16（上一层池化层输出就是深度16），采用32个卷积核，卷积核5*5的
        self.conv2 = nn.Conv2d(16, 32, 5)

        # 第二个池化层的定义：池化核2*2，步长2
        self.pool2 = nn.MaxPool2d(2, 2)

        # 最后三层都是全连接层：全连接层的输入都是一维的向量。
        # 上一层的输出output(32, 5, 5)，于是就把深度为32的5*5矩阵展成一维向量。lenet的第一层全连接层为120个结点。
        self.fc1 = nn.Linear(32*5*5, 120)
        # 第二个全连接层的输入就是第一层输出的120个结点，全连接层节点为84
        self.fc2 = nn.Linear(120, 84)
        # 第三个全连接层的输入就是第二层输出的84个结点，全连接层节点为10（实际是根据训练集进行设置的）
        self.fc3 = nn.Linear(84, 10)

    # 定义正向传播的过程。
    # 当参数x传递过来之后， 就会按照下边的过程进行前向传播。
    # 数据x的内容是：[batch（批大小），channel（深度），height（高），width]
    def forward(self, x):
        # 卷积之后图片的尺寸大小 N=（W-F+2P）/S+1，
        # 我们使用的图片是32*32的，于是W=32，卷积核维度5*5，于是F=5，padding=0，P=0，步长S默认1
        # output(16（使用16个卷积核，因此输出的深度为16）, 28（图片的高度）, 28（图片的宽度）)
        # 第一卷积层的输出还要经过Relu的激活函数进行非线性处理。
        x = F.relu(self.conv1(x))    # input(3, 32, 32)，output(16, 28, 28)

        # 上一层的输出为 output(16, 28, 28)，经过最大下采样之后变成了 output(16, 14, 14)。
        # 因为池化层不改变深度，只改变矩阵的大小，而且由于池化核2*2（也就是每2*2的一小块形成一个1*1的，所以把28变成了14*14的）
        x = self.pool1(x)            # output(16, 14, 14)，此时图片特征矩阵14*14

        # 卷积之后图片的尺寸大小 N=（W-F+2P）/S+1=（14-5+0）/1+1=10
        # 使用32个卷积核，因此输出的深度为32。图片尺寸10*10
        x = F.relu(self.conv2(x))    # output(32, 10, 10)

        # 输出，上一层的深度为32因此不变。原来矩阵10*10，经过2*2的变化成为5*5的矩阵。
        x = self.pool2(x)            # output(32, 5, 5)

        # 三个全连接层。
        # view（-1表示第一个维度的大小让电脑根据实际情况给我们计算，展平后的节点个数）进行把矩阵展开为一维数组。
        # -1在这里的意思是让电脑帮我们计算，比如:总长度是20，我们不想自己算20/5=4，就可以在不想算的位置放上-1，电脑就会自己计算对应的数字，
        #   a = torch.arange(0,20)		#此时a的shape是(1,20)
        #   a.view(4,5).shape		    #输出为(4,5)
        #   a.view(-1,5).shape	        #输出为(4,5)
        x = x.view(-1, 32*5*5)       # output(32*5*5)

        # 第一个全连接层输出经过激活函数
        x = F.relu(self.fc1(x))      # output(120)
        # 第二个全连接层输出经过激活函数
        x = F.relu(self.fc2(x))      # output(84)
        # 第三个全连接层输出（最后一层没有加入softmax是因为他计算交叉熵的时候内部处理过了）
        x = self.fc3(x)              # output(10)
        return x

# # 测试
# import torch
# input1 = torch.rand([32,3,32,32])  # [batch，channel，height，width]
# model = LeNet() # 实例化模型
# print(model)    # 输出一下模型
# output = model(input1)  # 将数据放入模型进行正向传播