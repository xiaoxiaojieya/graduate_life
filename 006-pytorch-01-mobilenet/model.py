from torch import nn
import torch

# 这个方法是官方的方法：它的作用是：将矩阵的深度ch调整为divisor的整数倍。
# It ensures that all layers have a channel number that is divisible by 8
# ch：卷积核个数，divisor：基数，min_ch：最小通道数，为None的时候divisor=8。
def _make_divisible(ch, divisor=8, min_ch=None):
    """
    This function is taken from the original tf repo.
    It ensures that all layers have a channel number that is divisible by 8
    It can be seen here:
    https://github.com/tensorflow/models/blob/master/research/slim/nets/mobilenet/mobilenet.py
    """
    # min_ch=None的时候，min_ch = divisor
    if min_ch is None:
        min_ch = divisor
    # 这个操作就类似于四舍五入的操作，他就可以把ch调整到距离8最近的整数倍的位置。
    # // 就是自动向下取整。
    new_ch = max(min_ch, int(ch + divisor / 2) // divisor * divisor)
    # 确保向下取整的时候，丢失不会超过10%。
    if new_ch < 0.9 * ch:
        new_ch += divisor
    return new_ch

# 定义残差结构中的每一层：卷积+BN+ReLU6
# 因为：倒残差的每一层都是这三个组成的，只不过最后一层的激活函数用的Linear，而不是ReLU6。
class ConvBNReLU(nn.Sequential):
    # groups的作用：当我们的groups=1的时候就是我们的普通卷积，groups=in_channel的时候就是DW卷积。
    def __init__(self, in_channel, out_channel, kernel_size=3, stride=1, groups=1):
        # 填充参数
        padding = (kernel_size - 1) // 2

        # 卷积+BN+ReLU6
        super(ConvBNReLU, self).__init__(
            # DW卷积也是调用的Conv2d，根据groups进行区分的。
            # 使用BN正则化，所以bias不起作用。
            nn.Conv2d(in_channel, out_channel, kernel_size, stride, padding, groups=groups, bias=False),
            nn.BatchNorm2d(out_channel),
            nn.ReLU6(inplace=True)
        )

# 倒残差结构的定义。
class InvertedResidual(nn.Module):
    # expand_ratio是扩展因子，也就是深度的扩大的倍率。
    # 这里的out_channel是整个残差结构的最终输出（而不是残差中的每一小层的输出），
    #   于是第一层的输出就要手动计算了hidden_channel。
    def __init__(self, in_channel, out_channel, stride, expand_ratio):
        super(InvertedResidual, self).__init__()
        # hidden_channel就是我们第一层的卷积层的个数，他是通过倍率与in_channel进行得到的。
        hidden_channel = in_channel * expand_ratio
        # use_shortcut是否使用捷径分支，使用的条件就是：S=1 && 输入与输出channel相同
        self.use_shortcut = stride == 1 and in_channel == out_channel

        layers = []
        # 当 t=1的时候，bottleneck不要第一层的1x1的卷积层（源码就是这样的）。
        if expand_ratio != 1:
            # 形成倒残差的第一层 1x1的卷积层。
            layers.append(ConvBNReLU(in_channel, hidden_channel, kernel_size=1))
        # extend与append一样，只不过extend可以一次插入很多函数。
        layers.extend([
            # 倒残差的第二层 3x3的卷积层。groups用来表示DW卷积
            ConvBNReLU(hidden_channel, hidden_channel, stride=stride, groups=hidden_channel),
            # 倒残差的第三层 1x31的卷积层。使用线性激活函数，因此这里就不能用ConvBNReLU了。
            nn.Conv2d(hidden_channel, out_channel, kernel_size=1, bias=False),
            nn.BatchNorm2d(out_channel),
            # 我们最后没有添加激活函数，默认的就是Linear激活函数了。
        ])

        # 最后将形成的残差结构传递过去，并且打包在一起。*表示这是一个列表。
        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        # 在初始化中已经定义过use_shortcut了
        if self.use_shortcut:
            # 用捷径分支了，最终返回的是：主分支的结果self.conv(x)+捷径分支的内容x
            return x + self.conv(x)
        else:
            return self.conv(x)

# MobileNetV2的网络结构。
class MobileNetV2(nn.Module):
    # alpha是超参数，用于控制conv的卷积核个数的倍率
    def __init__(self, num_classes=1000, alpha=1.0, round_nearest=8):
        super(MobileNetV2, self).__init__()
        # 定义倒残差结构
        block = InvertedResidual
        # input_channel是MobileNetV2的网络结构第一层卷积核的个数，也就是第二层卷积核输入的深度。
        # 我们看网络结构第一层的输出深度为32，因此在乘上他的倍率α
        # _make_divisible的作用是把卷积核的个数调整为round_nearest的整数倍，目的是为了设备的适应。
        input_channel = _make_divisible(32 * alpha, round_nearest)
        # last_channel是MobileNetV2的网络结构最后一层输出的深度，是1*1*1280
        last_channel = _make_divisible(1280 * alpha, round_nearest)
        # 这个列表存储的是MobileNetV2中的每一个倒残差的数据。
        # t：深度调整的扩展因子，c：输出的Channel，n：bottleneck重复的次数，s：步距
        inverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [6, 24, 2, 2],
            [6, 32, 3, 2],
            [6, 64, 4, 2],
            [6, 96, 3, 1],
            [6, 160, 3, 2],
            [6, 320, 1, 1],
        ]

        # 网络的结构存储在features中。
        features = []
        # 第一个卷积层：in_channel=3：彩色RGB，out_channel=input_channel上边求得的第一层卷积核的个数
        features.append(ConvBNReLU(3, input_channel, stride=2))


        # 遍历倒残差的参数列表，得到每一个到残差。
        for t, c, n, s in inverted_residual_setting:
            # 先调整卷积核的个数。
            output_channel = _make_divisible(c * alpha, round_nearest)
            # 每一个倒残差结构会有循环的次数，就在这里实现。
            # 注意：步距s只是针对重多次复的bootleneck中的第一个，其他的的步距都是1。
            for i in range(n):
                # 如果是第一个残差结构就把s=s,其他的都设置为1。
                stride = s if i == 0 else 1
                # block就是倒残差结构。
                features.append(block(input_channel, output_channel, stride, expand_ratio=t))
                # 更新输入的channel
                input_channel = output_channel
        # 所有的bottleneck都已经定义完了。

        # 再加入一个1x1的卷积层。这一层的输出channel就是last_channel=1280了
        features.append(ConvBNReLU(input_channel, last_channel, 1))
        # 特征提取层就完成了。
        # 打包特征提取层。
        self.features = nn.Sequential(*features)


        # 分类器部分
        # 自适应平均下采样。
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        # 全连接层。
        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(last_channel, num_classes)
        )

        # weight initialization
        for m in self.modules():
            # 卷积层的初始化
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out')
                if m.bias is not None:
                    nn.init.zeros_(m.bias)
            # BN的初始化
            elif isinstance(m, nn.BatchNorm2d):
                nn.init.ones_(m.weight)
                nn.init.zeros_(m.bias)
            # 线性分类器的初始化
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.zeros_(m.bias)

    # 正向传播过程。
    def forward(self, x):
        x = self.features(x)
        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x