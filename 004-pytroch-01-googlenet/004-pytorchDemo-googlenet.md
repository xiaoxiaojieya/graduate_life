## model.py

```python
import torch.nn as nn
import torch
import torch.nn.functional as F

# 首先将conv2d与relu打包为：BasicConv2d。
# 定义Inception模板。
# 定义辅助分类器 InceptionAux。
#


class GoogLeNet(nn.Module):
    # 初始化函数。
    # aux_logits=True是否使用辅助分类器。
    def __init__(self, num_classes=1000, aux_logits=True, init_weights=False):
        super(GoogLeNet, self).__init__()
        # 将是否使用分类器的变量设置到这个网络中。
        self.aux_logits = aux_logits

        # 卷积（pytorch默认向下取整）
        self.conv1 = BasicConv2d(3, 64, kernel_size=7, stride=2, padding=3)
        # 池化
        # ceil_mode=True：开启向上取整
        self.maxpool1 = nn.MaxPool2d(3, stride=2, ceil_mode=True)

        # 两个卷积
        self.conv2 = BasicConv2d(64, 64, kernel_size=1)
        self.conv3 = BasicConv2d(64, 192, kernel_size=3, padding=1)
        self.maxpool2 = nn.MaxPool2d(3, stride=2, ceil_mode=True)

        # inception3a与inception3b,参数：（深度，后边对应各个核的参数）
        self.inception3a = Inception(192, 64, 96, 128, 16, 32, 32)
        self.inception3b = Inception(256, 128, 128, 192, 32, 96, 64)
        self.maxpool3 = nn.MaxPool2d(3, stride=2, ceil_mode=True)

        # 我们的辅助分类器反正该后边进行搭建。
        # （虽然放在后边搭建，但是正向传播的时候，还是在正确的位置进行使用）
        self.inception4a = Inception(480, 192, 96, 208, 16, 48, 64)
        self.inception4b = Inception(512, 160, 112, 224, 24, 64, 64)
        self.inception4c = Inception(512, 128, 128, 256, 24, 64, 64)
        self.inception4d = Inception(512, 112, 144, 288, 32, 64, 64)
        self.inception4e = Inception(528, 256, 160, 320, 32, 128, 128)
        self.maxpool4 = nn.MaxPool2d(3, stride=2, ceil_mode=True)

        self.inception5a = Inception(832, 256, 160, 320, 32, 128, 128)
        self.inception5b = Inception(832, 384, 192, 384, 48, 128, 128)

        # 是否使用辅助分类器
        if self.aux_logits:
            # 由于aux1对应在4a后边，他的输入深度是4a输出的深度512.分的类数是num_classes
            self.aux1 = InceptionAux(512, num_classes)
            self.aux2 = InceptionAux(528, num_classes)

        # AdaptiveAvgPool2d可以把任意维度的特征矩阵转化为指定的矩阵（此处是1*1）
        # 上一步输出的是7*7*1024，因此最大下采样之后为 1*1*1024
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.dropout = nn.Dropout(0.4)
        # 然后输出num_classes类别的个数。
        self.fc = nn.Linear(1024, num_classes)
        if init_weights:
            self._initialize_weights()

    def forward(self, x):
        # N x 3 x 224 x 224
        x = self.conv1(x)
        # N x 64 x 112 x 112
        x = self.maxpool1(x)
        # N x 64 x 56 x 56
        x = self.conv2(x)
        # N x 64 x 56 x 56
        x = self.conv3(x)
        # N x 192 x 56 x 56
        x = self.maxpool2(x)

        # N x 192 x 28 x 28
        x = self.inception3a(x)
        # N x 256 x 28 x 28
        x = self.inception3b(x)
        # N x 480 x 28 x 28
        x = self.maxpool3(x)
        # N x 480 x 14 x 14
        x = self.inception4a(x)
        # N x 512 x 14 x 14
        # 只有训练模型才开启辅助分类器
        if self.training and self.aux_logits:    # eval model lose this layer
            # 然后从辅助分类器1得到一个分类结果。
            aux1 = self.aux1(x)

        x = self.inception4b(x)
        # N x 512 x 14 x 14
        x = self.inception4c(x)
        # N x 512 x 14 x 14
        x = self.inception4d(x)
        # N x 528 x 14 x 14
        if self.training and self.aux_logits:    # eval model lose this layer
            aux2 = self.aux2(x)

        x = self.inception4e(x)
        # N x 832 x 14 x 14
        x = self.maxpool4(x)
        # N x 832 x 7 x 7
        x = self.inception5a(x)
        # N x 832 x 7 x 7
        x = self.inception5b(x)
        # N x 1024 x 7 x 7

        x = self.avgpool(x)
        # 展平操作：N x 1024 x 1 x 1
        x = torch.flatten(x, 1)
        # N x 1024
        x = self.dropout(x)
        # 得到最终的输出
        x = self.fc(x)
        # 判断如果有辅助分类器就返回三个结果
        if self.training and self.aux_logits:   # eval model lose this layer
            return x, aux2, aux1
        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

# 定义Inception模板。因为这个模板多次被使用到。
class Inception(nn.Module):
    # 初始化参数。
    # in_channels：输入的特征矩阵的深度。
    # 定义网络结构。
    def __init__(self, in_channels, ch1x1, ch3x3red, ch3x3, ch5x5red, ch5x5, pool_proj):
        super(Inception, self).__init__()

        # 单独的 1*1 的 conv。ch1x1卷积核个数。
        self.branch1 = BasicConv2d(in_channels, ch1x1, kernel_size=1)

        # 先经过1*1的conv降维处理，在经过3*3的conv处理。
        self.branch2 = nn.Sequential(
            BasicConv2d(in_channels, ch3x3red, kernel_size=1),
            BasicConv2d(ch3x3red, ch3x3, kernel_size=3, padding=1)   # 保证输出大小等于输入大小
        )

        # 先经过1*1的conv降维处理，在经过5*5的conv处理。
        self.branch3 = nn.Sequential(
            BasicConv2d(in_channels, ch5x5red, kernel_size=1),
            BasicConv2d(ch5x5red, ch5x5, kernel_size=5, padding=2)   # 保证输出大小等于输入大小
        )

        # 先经过3*3的MaxPool处理，在经过1*1的conv降维处理。
        self.branch4 = nn.Sequential(
            nn.MaxPool2d(kernel_size=3, stride=1, padding=1),
            BasicConv2d(in_channels, pool_proj, kernel_size=1)
        )

    # 定义结构网络结构的正向传播。
    def forward(self, x):
        # 得到四个分支的输出。
        branch1 = self.branch1(x)
        branch2 = self.branch2(x)
        branch3 = self.branch3(x)
        branch4 = self.branch4(x)
        # 将分支都放在列表中。
        outputs = [branch1, branch2, branch3, branch4]
        # torch.cat将四个分支的值进行合并，合并维度为1的数据。
        return torch.cat(outputs, 1)

# 定义辅助分类器。
class InceptionAux(nn.Module):
    def __init__(self, in_channels, num_classes):
        super(InceptionAux, self).__init__()
        # 第一个平均下采样。
        self.averagePool = nn.AvgPool2d(kernel_size=5, stride=3)
        # 卷积层。
        self.conv = BasicConv2d(in_channels, 128, kernel_size=1)  # output[batch, 128, 4, 4]
        # FC-1024
        self.fc1 = nn.Linear(2048, 1024)
        # FC-num_classes
        self.fc2 = nn.Linear(1024, num_classes)

    def forward(self, x):
        # 输入：aux1:[N,512,14,14],aux2:[N,528,14,14]
        # 输出：aux1: N x 512 x 4 x 4, aux2: N x 528 x 4 x 4
        x = self.averagePool(x)
        # 卷积操作 输出：N x 128 x 4 x 4
        x = self.conv(x)
        # 展平数据 N x 128 x 4 x 4
        x = torch.flatten(x, 1)
        # 我们实例化一个model之后，可以控制model.train()与model.eval()来控制模型的状态
        # model.train()模式下self.training=True
        x = F.dropout(x, 0.5, training=self.training)
        # N x 2048
        # inplace=True指改变一个tensor的值的时候，直接在原来的内存上改变它的值。
        x = F.relu(self.fc1(x), inplace=True)
        x = F.dropout(x, 0.5, training=self.training)
        # N x 1024
        x = self.fc2(x)
        # N x num_classes
        return x

# 由于每次都把conv2d紧接着一个Relu，因此把他们打包在一起。
class BasicConv2d(nn.Module):
    # 初始化
    def __init__(self, in_channels, out_channels, **kwargs):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, **kwargs)
        self.relu = nn.ReLU(inplace=True)
    # 定义正向传播
    def forward(self, x):
        x = self.conv(x)
        x = self.relu(x)
        return x
```





## train.py

```python
import torch
import torch.nn as nn
from torchvision import transforms, datasets
import torchvision
import json
import matplotlib.pyplot as plt
import os
import torch.optim as optim
from model import GoogLeNet

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

data_transform = {
    "train": transforms.Compose([transforms.RandomResizedCrop(224),
                                 transforms.RandomHorizontalFlip(),
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]),
    "val": transforms.Compose([transforms.Resize((224, 224)),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])}

# data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))  # get data root path
data_root = os.path.abspath(os.getcwd())  # get data root path
image_path = data_root + "/data_set/flower_data/"  # flower data set path

train_dataset = datasets.ImageFolder(root=image_path + "train",
                                     transform=data_transform["train"])
train_num = len(train_dataset)

# {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
flower_list = train_dataset.class_to_idx
cla_dict = dict((val, key) for key, val in flower_list.items())
# write dict into json file
json_str = json.dumps(cla_dict, indent=4)
with open('class_indices.json', 'w') as json_file:
    json_file.write(json_str)

batch_size = 32
train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size=batch_size, shuffle=True,
                                           num_workers=0)

validate_dataset = datasets.ImageFolder(root=image_path + "val",
                                        transform=data_transform["val"])
val_num = len(validate_dataset)
validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                              batch_size=batch_size, shuffle=False,
                                              num_workers=0)

# test_data_iter = iter(validate_loader)
# test_image, test_label = test_data_iter.next()

# net = torchvision.models.googlenet(num_classes=5)
# model_dict = net.state_dict()
# pretrain_model = torch.load("googlenet.pth")
# del_list = ["aux1.fc2.weight", "aux1.fc2.bias",
#             "aux2.fc2.weight", "aux2.fc2.bias",
#             "fc.weight", "fc.bias"]
# pretrain_dict = {k: v for k, v in pretrain_model.items() if k not in del_list}
# model_dict.update(pretrain_dict)
# net.load_state_dict(model_dict)

# 得到网络
net = GoogLeNet(num_classes=5, aux_logits=True, init_weights=True)
net.to(device)
loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters(), lr=0.0003)

best_acc = 0.0
save_path = './googleNet.pth'
for epoch in range(30):
    # 开启训练
    net.train()
    running_loss = 0.0
    for step, data in enumerate(train_loader, start=0):
        images, labels = data
        optimizer.zero_grad()

        # 而得到三个返回的分类结果。
        logits, aux_logits2, aux_logits1 = net(images.to(device))
        # 分别计算三个结果的损失
        loss0 = loss_function(logits, labels.to(device))
        loss1 = loss_function(aux_logits1, labels.to(device))
        loss2 = loss_function(aux_logits2, labels.to(device))
        # 根据就一定的权重比来整合不同的损失
        loss = loss0 + loss1 * 0.3 + loss2 * 0.3

        # 向后传播得到梯度值
        loss.backward()
        # 根据梯度更新参数
        optimizer.step()

        running_loss += loss.item()
        # 打印训练过程
        rate = (step + 1) / len(train_loader)
        a = "*" * int(rate * 50)
        b = "." * int((1 - rate) * 50)
        print("\rtrain loss: {:^3.0f}%[{}->{}]{:.3f}".format(int(rate * 100), a, b, loss), end="")
    print()

    # 验证集
    net.eval()
    acc = 0.0  # accumulate accurate number / epoch
    with torch.no_grad():
        for val_data in validate_loader:
            val_images, val_labels = val_data
            # 输入图片信息，得到验证结果
            outputs = net(val_images.to(device))  # eval model only have last output layer
            predict_y = torch.max(outputs, dim=1)[1]
            acc += (predict_y == val_labels.to(device)).sum().item()
        val_accurate = acc / val_num
        if val_accurate > best_acc:
            best_acc = val_accurate
            torch.save(net.state_dict(), save_path)
        print('[epoch %d] train_loss: %.3f  test_accuracy: %.3f' %
              (epoch + 1, running_loss / step, val_accurate))

print('Finished Training')
```



## predict.py

```python
import torch
from model import GoogLeNet
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json

data_transform = transforms.Compose(
    [transforms.Resize((224, 224)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# load image
img = Image.open("../tulip.jpg")
plt.imshow(img)
# [N, C, H, W]
img = data_transform(img)
# expand batch dimension
img = torch.unsqueeze(img, dim=0)

# read class_indict
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# 预测的时候我们不需要辅助分类器，因此aux_logits=False
model = GoogLeNet(num_classes=5, aux_logits=False)
# 加载最佳权重值
model_weight_path = "./googleNet.pth"
# 由于刚才训练的模型使用了辅助分类器，这里预测不使用的话会与模型对不上，
# 因此我们需要设置strict=False，让他不是那么严格的去使用模型。
# 得到的unexpected_keys一系列层都是辅助分类器的内容。
missing_keys, unexpected_keys = model.load_state_dict(torch.load(model_weight_path), strict=False)
model.eval()
with torch.no_grad():
    # 模型结果输出，squeeze把batch维度压缩掉
    output = torch.squeeze(model(img))
    # 得到每一个类别的预测概率
    predict = torch.softmax(output, dim=0)
    # argmax(predict)获取概率最大处所对应的索引值。
    predict_cla = torch.argmax(predict).numpy()
print(class_indict[str(predict_cla)])
plt.show()
```

​       