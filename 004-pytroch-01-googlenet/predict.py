import torch
from model import GoogLeNet
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json

data_transform = transforms.Compose(
    [transforms.Resize((224, 224)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# load image
img = Image.open("../tulip.jpg")
plt.imshow(img)
# [N, C, H, W]
img = data_transform(img)
# expand batch dimension
img = torch.unsqueeze(img, dim=0)

# read class_indict
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# 预测的时候我们不需要辅助分类器，因此aux_logits=False
model = GoogLeNet(num_classes=5, aux_logits=False)
# 加载最佳权重值
model_weight_path = "./googleNet.pth"
# 由于刚才训练的模型使用了辅助分类器，这里预测不使用的话会与模型对不上，
# 因此我们需要设置strict=False，让他不是那么严格的去使用模型。
# 得到的unexpected_keys一系列层都是辅助分类器的内容。
missing_keys, unexpected_keys = model.load_state_dict(torch.load(model_weight_path), strict=False)
model.eval()
with torch.no_grad():
    # 模型结果输出，squeeze把batch维度压缩掉
    output = torch.squeeze(model(img))
    # 得到每一个类别的预测概率
    predict = torch.softmax(output, dim=0)
    # argmax(predict)获取概率最大处所对应的索引值。
    predict_cla = torch.argmax(predict).numpy()
print(class_indict[str(predict_cla)])
plt.show()