## model.py

```python
import torch.nn as nn
import torch

# 每一个Conv_x层都是一个由许多残差结构形成的层。其中基本上每一个Conv_x层的第一个残差结构都是虚线的，
#   之后的每一个都是实线的。我们把虚线的称之为下采样层。


# 18~34层的ResNet的残差层结构的定义。
# 简单-残差层的定义（它既有实线的残差结构的功能，又可以拓展为虚线的残差结构的功能）。
class BasicBlock(nn.Module):
    # 这个参数用于记录我们的残差中卷积核的个数变化。
    # 这里的每一层卷积层的个数没有变化
    expansion = 1
    # in_channel输入矩阵的深度，out_channel输出矩阵的深度（对应于主分支的卷积核的个数）。
    # downsample下采样，对应于存在虚线的残差结构。
    def __init__(self, in_channel, out_channel, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        # 卷积。stride=1的时候对应于实线的残差结构，stride=2的时候对应于虚线的残差结构。
        self.conv1 = nn.Conv2d(in_channels=in_channel, out_channels=out_channel,
                               kernel_size=3, stride=stride, padding=1, bias=False)
        # bn标准化：在参数里面只加上待处理的数据的通道数
        self.bn1 = nn.BatchNorm2d(out_channel)
        # 激活函数
        self.relu = nn.ReLU()

        # 卷积
        self.conv2 = nn.Conv2d(in_channels=out_channel, out_channels=out_channel,
                               kernel_size=3, stride=1, padding=1, bias=False)
        # bn标准化
        self.bn2 = nn.BatchNorm2d(out_channel)

        # 下采样方法
        self.downsample = downsample

    def forward(self, x):
        # 首先使用identity保存一下输入值，作为捷径位置上输出值。用于最后与主干的整合。
        identity = x
        # 判断有没有定义下采样，下采样为None的时候对应于实线的结构。
        if self.downsample is not None:
            # 有下采样的时候，就是虚线对应的结构的分支。
            # 于是我们传入下采样函数得到下采样的输出。
            identity = self.downsample(x)

        #
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        # 加上我们的 identity
        out += identity
        out = self.relu(out)

        return out

# 定义ResNet-50/101/152网络的残差层。
class Bottleneck(nn.Module):
    # 这个参数用于记录我们的残差中卷积核的个数变化。
    # 这里的最后一层卷积核个数是前边层数的4倍。
    expansion = 4

    def __init__(self, in_channel, out_channel, stride=1, downsample=None):
        super(Bottleneck, self).__init__()

        # 实线与虚线第一层的stride都是1，因此这一层无法区分。
        self.conv1 = nn.Conv2d(in_channels=in_channel, out_channels=out_channel,
                               kernel_size=1, stride=1, bias=False)
        self.bn1 = nn.BatchNorm2d(out_channel)

        # 第二层中实线的s=1，虚线的s=2，因此使用这个stride来区分实线还是虚线。
        self.conv2 = nn.Conv2d(in_channels=out_channel, out_channels=out_channel,
                               kernel_size=3, stride=stride, bias=False, padding=1)
        self.bn2 = nn.BatchNorm2d(out_channel)

        # 最后一层的卷积核个数是前边层的个数的4倍，因此为：out_channels=out_channel*self.expansion。
        self.conv3 = nn.Conv2d(in_channels=out_channel, out_channels=out_channel*self.expansion,
                               kernel_size=1, stride=1, bias=False)  # unsqueeze channels
        self.bn3 = nn.BatchNorm2d(out_channel*self.expansion)
        self.relu = nn.ReLU(inplace=True)
        # 下采样层的定义。
        self.downsample = downsample

    # 与上边一样。
    def forward(self, x):
        identity = x
        if self.downsample is not None:
            identity = self.downsample(x)

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        out += identity
        out = self.relu(out)

        return out

# 网络的框架的定义。
class ResNet(nn.Module):

    # block是不同的残差结构。blocks_num是某一个Conv_x中使用残差结构的数目（他是一个列表），比如：34层的就是[3，4，6，3]。
    # include_top是一个拓展位，它可以在这个网络的基础上搭建更加复杂的网络（实现了，但是本节课没有用）。
    def __init__(self, block, blocks_num, num_classes=1000, include_top=True):
        super(ResNet, self).__init__()
        self.include_top = include_top
        # 输入特征矩阵，他是通过MaxPool之后得到的深度。
        self.in_channel = 64
        # 第一层卷积层 7*7*64
        self.conv1 = nn.Conv2d(3, self.in_channel, kernel_size=7, stride=2,
                               padding=3, bias=False)
        # 将输出标准化。
        self.bn1 = nn.BatchNorm2d(self.in_channel)
        self.relu = nn.ReLU(inplace=True)
        # 最大池化
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        # 各个层中的一系列的残差结构。通过_make_layer函数来生成残差层。
        # Conv2_x 的生成。
        self.layer1 = self._make_layer(block, 64, blocks_num[0])
        # Conv3_x 的生成。
        self.layer2 = self._make_layer(block, 128, blocks_num[1], stride=2)
        # Conv4_x 的生成。
        self.layer3 = self._make_layer(block, 256, blocks_num[2], stride=2)
        # Conv5_x 的生成。
        self.layer4 = self._make_layer(block, 512, blocks_num[3], stride=2)

        # 网络经过残差之后的一些结构处理。
        # include_top是一个拓展位，这里默认为True。
        if self.include_top:
            # 池化
            self.avgpool = nn.AdaptiveAvgPool2d((1, 1))  # 1*1
            # 全连接：因为avgpool之后输出为1*1了，因此数量就是深度了。可以直接使用深度作为展平之后的维度。
            #   512 * block.expansion ：
            #       因为18/34最后一个Conv_x的输出为512。
            #       50/101/152最后一个Conv_x的输出为2048。
            self.fc = nn.Linear(512 * block.expansion, num_classes)

        # 对卷积层进行初始化操作。
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')

    # 残差层的生成。
    # block判断使用哪一个残差结构。
    # channel是每一个残差结构的第一层的深度（因为18/34层的每一个残差结构的每一层深度都相同，
    #     但是50/101/152的最后一层都是第一层的4倍）
    # block_num对应于每一个残差结构中有多少残差层。
    def _make_layer(self, block, channel, block_num, stride=1):
        # 对于18/34层的，downsample是空，则对应于实线的残差结构。
        downsample = None

        # 根据条件生成下采样结构（下采样结构就是具有虚线的结构），。
        # stride != 1 ：
        #   第一层没有送入stride，因此stride=1。
        #   第二层stride=2。
        #   第三层stride=2。
        #   第四层stride=2。
        # expansion在18/34中为1，50/101/152中为4.

        # 规律：8/34的第一个Conv_x无虚线且stride=1，并且从第二个Conv_x开始需要使用虚线的stride都是2。
        #      50/101/152层从第一个Conv_x开始是用虚线，并且使用虚线的：输出深度=输入深度的4倍。
        # 用这个规律就可以来判断 每一个Conv_x的第一个结点 是否需要生成下采样结构（也就是虚线结构）。
        if stride != 1 or self.in_channel != channel * block.expansion:
            # 生成下采样（虚线结构）。
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channel, channel * block.expansion, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(channel * block.expansion))

        # layers用来保存当前Conv_x的每一个残差结构。
        layers = []
        # 这个是针对每一个 Conv_x 的第一个残差结构。如果通过上边的if生成了下采样的话downsample就不为空，否则为空。
        # 然后在调用 block() 生成每一个Conv_x的第一个残差结构，如果为空的话就是生成一个普通的残差。
        layers.append(block(self.in_channel, channel, downsample=downsample, stride=stride))

        # 得到Conv_x第一层残差对应的输出的深度。
        self.in_channel = channel * block.expansion

        # 然后把后几层添加进去（因为所有网络的第2层开始往后都是实线的，所以直接加入就可以了）
        # range默认是从0开始的，因此这里需要指定从1开始。
        for _ in range(1, block_num):
            layers.append(block(self.in_channel, channel))

        # 然后返回 layers 的序列即可生成我们的指定的每一个Conv_x层。
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        if self.include_top:
            x = self.avgpool(x)
            x = torch.flatten(x, 1)
            x = self.fc(x)

        return x

# 也适用于18，只不过要把[3, 4, 6, 3]输入的时候换成[2,2,2,2]
def resnet34(num_classes=1000, include_top=True):
    return ResNet(BasicBlock, [3, 4, 6, 3], num_classes=num_classes, include_top=include_top)

# 也适用于50/152，只不过要更换列表参数即可。
def resnet101(num_classes=1000, include_top=True):
    return ResNet(Bottleneck, [3, 4, 23, 3], num_classes=num_classes, include_top=include_top)
```





## train.py

```python
import torch
import torch.nn as nn
from torchvision import transforms, datasets
import json
import matplotlib.pyplot as plt
import os
import torch.optim as optim
from model import resnet34, resnet101


# 要使用迁移学习的方法进行训练。我们进入resnet源码可以找到它的预训练模型的连接。
import torchvision.models.resnet
# 我们这里使用34的，去浏览器下载。
# model_urls = {
#     'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
#     'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
#     'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
#     'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
#     'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
#     'resnext50_32x4d': 'https://download.pytorch.org/models/resnext50_32x4d-7cdf4587.pth',
#     'resnext101_32x8d': 'https://download.pytorch.org/models/resnext101_32x8d-8ba56ff5.pth',
# }


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

data_transform = {
    "train": transforms.Compose([transforms.RandomResizedCrop(224),
                                 transforms.RandomHorizontalFlip(),
                                 transforms.ToTensor(),
                                 # 这里的标准化处理参数都是用的官网推荐的
                                 transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]),
    "val": transforms.Compose([transforms.Resize(256),
                               transforms.CenterCrop(224),
                               transforms.ToTensor(),
                               transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])}


# data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))  # get data root path
data_root = os.path.abspath(os.getcwd())  # get data root path
image_path = data_root + "/data_set/flower_data/"  # flower data set path

train_dataset = datasets.ImageFolder(root=image_path+"train",
                                     transform=data_transform["train"])
train_num = len(train_dataset)

# {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
flower_list = train_dataset.class_to_idx
cla_dict = dict((val, key) for key, val in flower_list.items())
# write dict into json file
json_str = json.dumps(cla_dict, indent=4)
with open('class_indices.json', 'w') as json_file:
    json_file.write(json_str)

batch_size = 16
train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size=batch_size, shuffle=True,
                                           num_workers=0)

validate_dataset = datasets.ImageFolder(root=image_path + "val",
                                        transform=data_transform["val"])
val_num = len(validate_dataset)
validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                              batch_size=batch_size, shuffle=False,
                                              num_workers=0)


# 实例化34网络，直接是具有1000个输出结点的
net = resnet34()

#==============  迁移学习   =============
# 加载我们从别处的得到的权重。
model_weight_path = "./resnet34-pre.pth"
# load_state_dict载入权重
missing_keys, unexpected_keys = net.load_state_dict(torch.load(model_weight_path), strict=False)
# for param in net.parameters():
#     param.requires_grad = False
# change fc layer structure

# 这个是得到我们模型的全连接层的输出数据（因为他是1000个）。
inchannel = net.fc.in_features
# 然后我们在最后的全连接层中再加入一个 全连接层，让1000个输出转化为 5个输出。
net.fc = nn.Linear(inchannel, 5)
net.to(device)
#==============  迁移学习   =============

loss_function = nn.CrossEntropyLoss()
optimizer = optim.Adam(net.parameters(), lr=0.0001)

best_acc = 0.0
save_path = './resNet34.pth'
for epoch in range(3):
    # train
    net.train()
    running_loss = 0.0
    for step, data in enumerate(train_loader, start=0):
        images, labels = data
        optimizer.zero_grad()
        logits = net(images.to(device))
        loss = loss_function(logits, labels.to(device))
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.item()
        # print train process
        rate = (step+1)/len(train_loader)
        a = "*" * int(rate * 50)
        b = "." * int((1 - rate) * 50)
        print("\rtrain loss: {:^3.0f}%[{}->{}]{:.4f}".format(int(rate*100), a, b, loss), end="")
    print()

    # validate
    net.eval()
    acc = 0.0  # accumulate accurate number / epoch
    with torch.no_grad():
        for val_data in validate_loader:
            val_images, val_labels = val_data
            outputs = net(val_images.to(device))  # eval model only have last output layer
            # loss = loss_function(outputs, test_labels)
            predict_y = torch.max(outputs, dim=1)[1]
            acc += (predict_y == val_labels.to(device)).sum().item()
        val_accurate = acc / val_num
        if val_accurate > best_acc:
            best_acc = val_accurate
            torch.save(net.state_dict(), save_path)
        print('[epoch %d] train_loss: %.3f  test_accuracy: %.3f' %
              (epoch + 1, running_loss / step, val_accurate))

print('Finished Training')
```



## predict.py

```python
import torch
from model import resnet34
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json


data_transform = transforms.Compose(
    [transforms.Resize(256),
     transforms.CenterCrop(224),
     transforms.ToTensor(),
     # 特别注意标准化的参数需要与迁移学习的权重一样。
     transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])

# load image
img = Image.open("../tulip.jpg")
plt.imshow(img)
# [N, C, H, W]
img = data_transform(img)
# expand batch dimension
img = torch.unsqueeze(img, dim=0)

# read class_indict
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# create model
model = resnet34(num_classes=5)
# load model weights
model_weight_path = "./resNet34.pth"
model.load_state_dict(torch.load(model_weight_path))
model.eval()
with torch.no_grad():
    # predict class
    output = torch.squeeze(model(img))
    predict = torch.softmax(output, dim=0)
    predict_cla = torch.argmax(predict).numpy()
print(class_indict[str(predict_cla)], predict[predict_cla].numpy())
plt.show()
```

​       