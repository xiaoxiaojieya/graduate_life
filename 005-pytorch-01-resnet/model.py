import torch.nn as nn
import torch

# 每一个Conv_x层都是一个由许多残差结构形成的层。其中基本上每一个Conv_x层的第一个残差结构都是虚线的，
#   之后的每一个都是实线的。我们把虚线的称之为下采样层。


# 18~34层的ResNet的残差层结构的定义。
# 简单-残差层的定义（它既有实线的残差结构的功能，又可以拓展为虚线的残差结构的功能）。
class BasicBlock(nn.Module):
    # 这个参数用于记录我们的残差中卷积核的个数变化。
    # 这里的每一层卷积层的个数没有变化
    expansion = 1
    # in_channel输入矩阵的深度，out_channel输出矩阵的深度（对应于主分支的卷积核的个数）。
    # downsample下采样，对应于存在虚线的残差结构。
    def __init__(self, in_channel, out_channel, stride=1, downsample=None):
        super(BasicBlock, self).__init__()
        # 卷积。stride=1的时候对应于实线的残差结构，stride=2的时候对应于虚线的残差结构。
        self.conv1 = nn.Conv2d(in_channels=in_channel, out_channels=out_channel,
                               kernel_size=3, stride=stride, padding=1, bias=False)
        # bn标准化：在参数里面只加上待处理的数据的通道数
        self.bn1 = nn.BatchNorm2d(out_channel)
        # 激活函数
        self.relu = nn.ReLU()

        # 卷积
        self.conv2 = nn.Conv2d(in_channels=out_channel, out_channels=out_channel,
                               kernel_size=3, stride=1, padding=1, bias=False)
        # bn标准化
        self.bn2 = nn.BatchNorm2d(out_channel)

        # 下采样方法
        self.downsample = downsample

    def forward(self, x):
        # 首先使用identity保存一下输入值，作为捷径位置上输出值。用于最后与主干的整合。
        identity = x
        # 判断有没有定义下采样，下采样为None的时候对应于实线的结构。
        if self.downsample is not None:
            # 有下采样的时候，就是虚线对应的结构的分支。
            # 于是我们传入下采样函数得到下采样的输出。
            identity = self.downsample(x)

        #
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        # 加上我们的 identity
        out += identity
        out = self.relu(out)

        return out

# 定义ResNet-50/101/152网络的残差层。
class Bottleneck(nn.Module):
    # 这个参数用于记录我们的残差中卷积核的个数变化。
    # 这里的最后一层卷积核个数是前边层数的4倍。
    expansion = 4

    def __init__(self, in_channel, out_channel, stride=1, downsample=None):
        super(Bottleneck, self).__init__()

        # 实线与虚线第一层的stride都是1，因此这一层无法区分。
        self.conv1 = nn.Conv2d(in_channels=in_channel, out_channels=out_channel,
                               kernel_size=1, stride=1, bias=False)
        self.bn1 = nn.BatchNorm2d(out_channel)

        # 第二层中实线的s=1，虚线的s=2，因此使用这个stride来区分实线还是虚线。
        self.conv2 = nn.Conv2d(in_channels=out_channel, out_channels=out_channel,
                               kernel_size=3, stride=stride, bias=False, padding=1)
        self.bn2 = nn.BatchNorm2d(out_channel)

        # 最后一层的卷积核个数是前边层的个数的4倍，因此为：out_channels=out_channel*self.expansion。
        self.conv3 = nn.Conv2d(in_channels=out_channel, out_channels=out_channel*self.expansion,
                               kernel_size=1, stride=1, bias=False)  # unsqueeze channels
        self.bn3 = nn.BatchNorm2d(out_channel*self.expansion)
        self.relu = nn.ReLU(inplace=True)
        # 下采样层的定义。
        self.downsample = downsample

    # 与上边一样。
    def forward(self, x):
        identity = x
        if self.downsample is not None:
            identity = self.downsample(x)

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        out += identity
        out = self.relu(out)

        return out

# 网络的框架的定义。
class ResNet(nn.Module):

    # block是不同的残差结构。blocks_num是某一个Conv_x中使用残差结构的数目（他是一个列表），比如：34层的就是[3，4，6，3]。
    # include_top是一个拓展位，它可以在这个网络的基础上搭建更加复杂的网络（实现了，但是本节课没有用）。
    def __init__(self, block, blocks_num, num_classes=1000, include_top=True):
        super(ResNet, self).__init__()
        self.include_top = include_top
        # 输入特征矩阵，他是通过MaxPool之后得到的深度。
        self.in_channel = 64
        # 第一层卷积层 7*7*64
        self.conv1 = nn.Conv2d(3, self.in_channel, kernel_size=7, stride=2,
                               padding=3, bias=False)
        # 将输出标准化。
        self.bn1 = nn.BatchNorm2d(self.in_channel)
        self.relu = nn.ReLU(inplace=True)
        # 最大池化
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        # 各个层中的一系列的残差结构。通过_make_layer函数来生成残差层。
        # Conv2_x 的生成。
        self.layer1 = self._make_layer(block, 64, blocks_num[0])
        # Conv3_x 的生成。
        self.layer2 = self._make_layer(block, 128, blocks_num[1], stride=2)
        # Conv4_x 的生成。
        self.layer3 = self._make_layer(block, 256, blocks_num[2], stride=2)
        # Conv5_x 的生成。
        self.layer4 = self._make_layer(block, 512, blocks_num[3], stride=2)

        # 网络经过残差之后的一些结构处理。
        # include_top是一个拓展位，这里默认为True。
        if self.include_top:
            # 池化
            self.avgpool = nn.AdaptiveAvgPool2d((1, 1))  # 1*1
            # 全连接：因为avgpool之后输出为1*1了，因此数量就是深度了。可以直接使用深度作为展平之后的维度。
            #   512 * block.expansion ：
            #       因为18/34最后一个Conv_x的输出为512。
            #       50/101/152最后一个Conv_x的输出为2048。
            self.fc = nn.Linear(512 * block.expansion, num_classes)

        # 对卷积层进行初始化操作。
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')

    # 残差层的生成。
    # block判断使用哪一个残差结构。
    # channel是每一个残差结构的第一层的深度（因为18/34层的每一个残差结构的每一层深度都相同，
    #     但是50/101/152的最后一层都是第一层的4倍）
    # block_num对应于每一个残差结构中有多少残差层。
    def _make_layer(self, block, channel, block_num, stride=1):
        # 对于18/34层的，downsample是空，则对应于实线的残差结构。
        downsample = None

        # 根据条件生成下采样结构（下采样结构就是具有虚线的结构），。
        # stride != 1 ：
        #   第一层没有送入stride，因此stride=1。
        #   第二层stride=2。
        #   第三层stride=2。
        #   第四层stride=2。
        # expansion在18/34中为1，50/101/152中为4.

        # 规律：8/34的第一个Conv_x无虚线且stride=1，并且从第二个Conv_x开始需要使用虚线的stride都是2。
        #      50/101/152层从第一个Conv_x开始是用虚线，并且使用虚线的：输出深度=输入深度的4倍。
        # 用这个规律就可以来判断 每一个Conv_x的第一个结点 是否需要生成下采样结构（也就是虚线结构）。
        if stride != 1 or self.in_channel != channel * block.expansion:
            # 生成下采样（虚线结构）。
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channel, channel * block.expansion, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(channel * block.expansion))

        # layers用来保存当前Conv_x的每一个残差结构。
        layers = []
        # 这个是针对每一个 Conv_x 的第一个残差结构。如果通过上边的if生成了下采样的话downsample就不为空，否则为空。
        # 然后在调用 block() 生成每一个Conv_x的第一个残差结构，如果为空的话就是生成一个普通的残差。
        layers.append(block(self.in_channel, channel, downsample=downsample, stride=stride))

        # 得到Conv_x第一层残差对应的输出的深度。
        self.in_channel = channel * block.expansion

        # 然后把后几层添加进去（因为所有网络的第2层开始往后都是实线的，所以直接加入就可以了）
        # range默认是从0开始的，因此这里需要指定从1开始。
        for _ in range(1, block_num):
            layers.append(block(self.in_channel, channel))

        # 然后返回 layers 的序列即可生成我们的指定的每一个Conv_x层。
        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        if self.include_top:
            x = self.avgpool(x)
            x = torch.flatten(x, 1)
            x = self.fc(x)

        return x

# 也适用于18，只不过要把[3, 4, 6, 3]输入的时候换成[2,2,2,2]
def resnet34(num_classes=1000, include_top=True):
    return ResNet(BasicBlock, [3, 4, 6, 3], num_classes=num_classes, include_top=include_top)

# 也适用于50/152，只不过要更换列表参数即可。
def resnet101(num_classes=1000, include_top=True):
    return ResNet(Bottleneck, [3, 4, 23, 3], num_classes=num_classes, include_top=include_top)