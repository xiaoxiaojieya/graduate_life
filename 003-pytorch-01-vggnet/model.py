import torch.nn as nn
import torch

# 我们尝试搭建多个vggnet模型。通过cfgs定义每一种网络的网络结构。
# 两部分：、
#   1，特征提取：也就是全连接层之前的网络。
#   2，分类网络结构：也就是全连接层。

class VGG(nn.Module):
    # 初始化函数：传入features网络结构，分类数量
    def __init__(self, features, num_classes=1000, init_weights=False):
        super(VGG, self).__init__()
        # 这里得到特征提取网络。
        self.features = features
        # 这里定义网络分类结构。
        self.classifier = nn.Sequential(
            nn.Dropout(p=0.5),
            # 其实是4096，但是这里为了测试方便，所以只用2048个。
            nn.Linear(512*7*7, 2048),
            nn.ReLU(True),
            nn.Dropout(p=0.5),
            nn.Linear(2048, 2048),
            nn.ReLU(True),
            nn.Linear(2048, num_classes)
        )
        # 是否初始化
        if init_weights:
            self._initialize_weights()

    def forward(self, x):
        # 特征提取
        x = self.features(x)
        # 展平数据，dim=0是batch
        x = torch.flatten(x, start_dim=1)
        # 分类
        x = self.classifier(x)
        return x

    # 初始化权重的函数。
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                # nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                nn.init.xavier_uniform_(m.weight)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.xavier_uniform_(m.weight)
                # nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

# 生成网络结构。
# 根据传入的cfg列表，生成对应的特征提取结构。
def make_features(cfg: list):
    # 生成的网络结构都存放在layers中。
    layers = []
    # channel都是3，RGB深度为3.
    in_channels = 3
    # 遍历列表，当列表的值为M的时候向layers中加入MaxPool2d，否则加入Conv2d
    for v in cfg:
        if v == "M":
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            # 每一个卷积操作就会进行一次Relu激活。
            layers += [conv2d, nn.ReLU(True)]
            # 每次经过一个卷积之后，输出的深度就会变成V（上一层的卷积核的个数）。
            in_channels = v
    # 最后使用Sequential将layers形成特征提取结构。
    # layers前边的*表示是通过非关键字的方式传递进去的（看源码的example）。
    # 因为Sequential有两种方法，一种是传入带关键字的，一种是不带关键字的。
    return nn.Sequential(*layers)

# 定义一个cfgs字典文件，他的每一个key是一个模型的配置名称，value是对应的配置信息。
cfgs = {
    # 数字代表卷积层的卷积核个数，字母M代表maxpool。
    'vgg11': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'vgg13': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'vgg16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'vgg19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}


# 实例化vggnet。通过传入的model_name选择对应的网络。
# **kwargs的意思是后边传入的参数是可变的。
def vgg(model_name="vgg16", **kwargs):
    try:
        # 得到对应网络的配置
        cfg = cfgs[model_name]
    except:
        print("Warning: model number {} not in cfgs dict!".format(model_name))
        exit(-1)
    # 得到网络模型。
    model = VGG(make_features(cfg), **kwargs)
    return model