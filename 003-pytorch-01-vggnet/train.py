import torch.nn as nn
from torchvision import transforms, datasets
import json
import os
import torch.optim as optim
from model import vgg
import torch

# *********************************************** #
# 别用本机训练这个数据集，会卡死的，可以在google上训练。  #
#             以后使用迁移网络来改进VGG               #
# *********************************************** #

# 选择设备
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# 数据的标准化处理
data_transform = {
    "train": transforms.Compose([transforms.RandomResizedCrop(224),  # 剪切为224*224
                                 transforms.RandomHorizontalFlip(),  # 水平翻转
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]),
    "val": transforms.Compose([transforms.Resize((224, 224)),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])}

# 数据的根目录
# data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))
data_root = os.path.abspath(os.getcwd())  # get data root path
# 数据的完整路径
image_path = data_root + "/data_set/flower_data/"

# 得到训练数据集
train_dataset = datasets.ImageFolder(root=image_path+"train",
                                     transform=data_transform["train"])
train_num = len(train_dataset)


# 得到训练集的数据分类。 {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
flower_list = train_dataset.class_to_idx
# 将训练集的index与value进行兑换。
cla_dict = dict((val, key) for key, val in flower_list.items())
# 将类别形成json格式
json_str = json.dumps(cla_dict, indent=4)
# 将json格式写入文件。
with open('class_indices.json', 'w') as json_file:
    json_file.write(json_str)

batch_size = 32
# 加载训练集
train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size=batch_size, shuffle=True,
                                           num_workers=0)

# 得到验证集
validate_dataset = datasets.ImageFolder(root=image_path + "val",
                                        transform=data_transform["val"])
val_num = len(validate_dataset)
# 加载验证集
validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                              batch_size=batch_size, shuffle=False,
                                              num_workers=0)

# test_data_iter = iter(validate_loader)
# test_image, test_label = test_data_iter.next()

# 要使用的模型
model_name = "vgg16"
# 传入参数进行模型的初始化
net = vgg(model_name=model_name, num_classes=5, init_weights=True)
net.to(device)
# 定义损失函数
loss_function = nn.CrossEntropyLoss()
# 得到优化器
optimizer = optim.Adam(net.parameters(), lr=0.0001)

# 最佳精确度
best_acc = 0.0
# 模型的值的保存路径
save_path = './{}Net.pth'.format(model_name)

# 进行30批的训练
for epoch in range(30):
    # 开启训练（dropout可用）
    net.train()
    # 损失率
    running_loss = 0.0
    # 从当前数据集中，枚举得到所有图片数据，以及训练的步数。
    # enumerate得到list:(0, seq[0]), (1, seq[1]), (2, seq[2]), ...
    for step, data in enumerate(train_loader, start=0):
        images, labels = data
        optimizer.zero_grad()
        # 得到结果
        outputs = net(images.to(device))
        # 计算损失函数
        loss = loss_function(outputs, labels.to(device))
        # 反向传播计算梯度值
        loss.backward()
        # 使用优化器更新参数
        optimizer.step()

        # 累加损失
        running_loss += loss.item()
        # 打印训练过程
        rate = (step + 1) / len(train_loader)
        a = "*" * int(rate * 50)
        b = "." * int((1 - rate) * 50)
        print("\rtrain loss: {:^3.0f}%[{}->{}]{:.3f}".format(int(rate * 100), a, b, loss), end="")
    print()

    # 测试集
    net.eval()
    acc = 0.0  # 准确度
    with torch.no_grad():
        # 将测试集的每一个数据进行测试，验证器准确性。
        for val_data in validate_loader:
            val_images, val_labels = val_data
            optimizer.zero_grad()
            # 训练的输出
            outputs = net(val_images.to(device))
            # 求出当前数据的最大预测值
            predict_y = torch.max(outputs, dim=1)[1]
            # 如果预测对了，就把acc+1
            acc += (predict_y == val_labels.to(device)).sum().item()
        # 求出正确率
        val_accurate = acc / val_num
        # 记录最佳的训练的权重
        if val_accurate > best_acc:
            best_acc = val_accurate
            torch.save(net.state_dict(), save_path)
        print('[epoch %d] train_loss: %.3f  test_accuracy: %.3f' %
              (epoch + 1, running_loss / step, val_accurate))

print('Finished Training')