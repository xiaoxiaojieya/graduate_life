import torch
from model import vgg
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json

# 输入图片得到转化
data_transform = transforms.Compose(
    [transforms.Resize((224, 224)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# 加载图片
img = Image.open("../tulip.jpg")
# 显示一下
plt.imshow(img)
# 进行图片的标准化 ==》[N, C, H, W]
img = data_transform(img)
# 增加Batch
img = torch.unsqueeze(img, dim=0)

# read class_indict
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# 创建一个模型
model = vgg(model_name="vgg16", num_classes=5)
# 得到最佳的模型的权重表路径
model_weight_path = "./vgg16Net.pth"
# 加载最佳的模型的权重表
model.load_state_dict(torch.load(model_weight_path))
# 关闭dropout
model.eval()
with torch.no_grad():
    # 得到训练结果
    output = torch.squeeze(model(img))
    # 得到训练结果的概率大小
    predict = torch.softmax(output, dim=0)
    # 得到最大概率的下标
    predict_cla = torch.argmax(predict).numpy()
# 输出最大预测的类别名字
print(class_indict[str(predict_cla)])
plt.show()