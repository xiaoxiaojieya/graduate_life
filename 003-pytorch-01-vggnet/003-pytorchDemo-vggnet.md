## model.py

```python
import torch.nn as nn
import torch

# 我们尝试搭建多个vggnet模型。通过cfgs定义每一种网络的网络结构。
# 两部分：、
#   1，特征提取：也就是全连接层之前的网络。
#   2，分类网络结构：也就是全连接层。

class VGG(nn.Module):
    # 初始化函数：传入features网络结构，分类数量
    def __init__(self, features, num_classes=1000, init_weights=False):
        super(VGG, self).__init__()
        # 这里得到特征提取网络。
        self.features = features
        # 这里定义网络分类结构。
        self.classifier = nn.Sequential(
            nn.Dropout(p=0.5),
            # 其实是4096，但是这里为了测试方便，所以只用2048个。
            nn.Linear(512*7*7, 2048),
            nn.ReLU(True),
            nn.Dropout(p=0.5),
            nn.Linear(2048, 2048),
            nn.ReLU(True),
            nn.Linear(2048, num_classes)
        )
        # 是否初始化
        if init_weights:
            self._initialize_weights()

    def forward(self, x):
        # 特征提取
        x = self.features(x)
        # 展平数据，dim=0是batch
        x = torch.flatten(x, start_dim=1)
        # 分类
        x = self.classifier(x)
        return x

    # 初始化权重的函数。
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                # nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                nn.init.xavier_uniform_(m.weight)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.xavier_uniform_(m.weight)
                # nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

# 生成网络结构。
# 根据传入的cfg列表，生成对应的特征提取结构。
def make_features(cfg: list):
    # 生成的网络结构都存放在layers中。
    layers = []
    # channel都是3，RGB深度为3.
    in_channels = 3
    # 遍历列表，当列表的值为M的时候向layers中加入MaxPool2d，否则加入Conv2d
    for v in cfg:
        if v == "M":
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            # 每一个卷积操作就会进行一次Relu激活。
            layers += [conv2d, nn.ReLU(True)]
            # 每次经过一个卷积之后，输出的深度就会变成V（上一层的卷积核的个数）。
            in_channels = v
    # 最后使用Sequential将layers形成特征提取结构。
    # layers前边的*表示是通过非关键字的方式传递进去的（看源码的example）。
    # 因为Sequential有两种方法，一种是传入带关键字的，一种是不带关键字的。
    return nn.Sequential(*layers)

# 定义一个cfgs字典文件，他的每一个key是一个模型的配置名称，value是对应的配置信息。
cfgs = {
    # 数字代表卷积层的卷积核个数，字母M代表maxpool。
    'vgg11': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'vgg13': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'vgg16': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'vgg19': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}


# 实例化vggnet。通过传入的model_name选择对应的网络。
# **kwargs的意思是后边传入的参数是可变的。
def vgg(model_name="vgg16", **kwargs):
    try:
        # 得到对应网络的配置
        cfg = cfgs[model_name]
    except:
        print("Warning: model number {} not in cfgs dict!".format(model_name))
        exit(-1)
    # 得到网络模型。
    model = VGG(make_features(cfg), **kwargs)
    return model
```





## train.py

```python
import torch.nn as nn
from torchvision import transforms, datasets
import json
import os
import torch.optim as optim
from model import vgg
import torch

# *********************************************** #
# 别用本机训练这个数据集，会卡死的，可以在google上训练。  #
#             以后使用迁移网络来改进VGG               #
# *********************************************** #

# 选择设备
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# 数据的标准化处理
data_transform = {
    "train": transforms.Compose([transforms.RandomResizedCrop(224),  # 剪切为224*224
                                 transforms.RandomHorizontalFlip(),  # 水平翻转
                                 transforms.ToTensor(),
                                 transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]),
    "val": transforms.Compose([transforms.Resize((224, 224)),
                               transforms.ToTensor(),
                               transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])}

# 数据的根目录
# data_root = os.path.abspath(os.path.join(os.getcwd(), "../.."))
data_root = os.path.abspath(os.getcwd())  # get data root path
# 数据的完整路径
image_path = data_root + "/data_set/flower_data/"

# 得到训练数据集
train_dataset = datasets.ImageFolder(root=image_path+"train",
                                     transform=data_transform["train"])
train_num = len(train_dataset)


# 得到训练集的数据分类。 {'daisy':0, 'dandelion':1, 'roses':2, 'sunflower':3, 'tulips':4}
flower_list = train_dataset.class_to_idx
# 将训练集的index与value进行兑换。
cla_dict = dict((val, key) for key, val in flower_list.items())
# 将类别形成json格式
json_str = json.dumps(cla_dict, indent=4)
# 将json格式写入文件。
with open('class_indices.json', 'w') as json_file:
    json_file.write(json_str)

batch_size = 32
# 加载训练集
train_loader = torch.utils.data.DataLoader(train_dataset,
                                           batch_size=batch_size, shuffle=True,
                                           num_workers=0)

# 得到验证集
validate_dataset = datasets.ImageFolder(root=image_path + "val",
                                        transform=data_transform["val"])
val_num = len(validate_dataset)
# 加载验证集
validate_loader = torch.utils.data.DataLoader(validate_dataset,
                                              batch_size=batch_size, shuffle=False,
                                              num_workers=0)

# test_data_iter = iter(validate_loader)
# test_image, test_label = test_data_iter.next()

# 要使用的模型
model_name = "vgg16"
# 传入参数进行模型的初始化
net = vgg(model_name=model_name, num_classes=5, init_weights=True)
net.to(device)
# 定义损失函数
loss_function = nn.CrossEntropyLoss()
# 得到优化器
optimizer = optim.Adam(net.parameters(), lr=0.0001)

# 最佳精确度
best_acc = 0.0
# 模型的值的保存路径
save_path = './{}Net.pth'.format(model_name)

# 进行30批的训练
for epoch in range(30):
    # 开启训练（dropout可用）
    net.train()
    # 损失率
    running_loss = 0.0
    # 从当前数据集中，枚举得到所有图片数据，以及训练的步数。
    # enumerate得到list:(0, seq[0]), (1, seq[1]), (2, seq[2]), ...
    for step, data in enumerate(train_loader, start=0):
        images, labels = data
        optimizer.zero_grad()
        # 得到结果
        outputs = net(images.to(device))
        # 计算损失函数
        loss = loss_function(outputs, labels.to(device))
        # 反向传播计算梯度值
        loss.backward()
        # 使用优化器更新参数
        optimizer.step()

        # 累加损失
        running_loss += loss.item()
        # 打印训练过程
        rate = (step + 1) / len(train_loader)
        a = "*" * int(rate * 50)
        b = "." * int((1 - rate) * 50)
        print("\rtrain loss: {:^3.0f}%[{}->{}]{:.3f}".format(int(rate * 100), a, b, loss), end="")
    print()

    # 测试集
    net.eval()
    acc = 0.0  # 准确度
    with torch.no_grad():
        # 将测试集的每一个数据进行测试，验证器准确性。
        for val_data in validate_loader:
            val_images, val_labels = val_data
            optimizer.zero_grad()
            # 训练的输出
            outputs = net(val_images.to(device))
            # 求出当前数据的最大预测值
            predict_y = torch.max(outputs, dim=1)[1]
            # 如果预测对了，就把acc+1
            acc += (predict_y == val_labels.to(device)).sum().item()
        # 求出正确率
        val_accurate = acc / val_num
        # 记录最佳的训练的权重
        if val_accurate > best_acc:
            best_acc = val_accurate
            torch.save(net.state_dict(), save_path)
        print('[epoch %d] train_loss: %.3f  test_accuracy: %.3f' %
              (epoch + 1, running_loss / step, val_accurate))

print('Finished Training')
```



## predict.py

```python
import torch
from model import vgg
from PIL import Image
from torchvision import transforms
import matplotlib.pyplot as plt
import json

# 输入图片得到转化
data_transform = transforms.Compose(
    [transforms.Resize((224, 224)),
     transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

# 加载图片
img = Image.open("../tulip.jpg")
# 显示一下
plt.imshow(img)
# 进行图片的标准化 ==》[N, C, H, W]
img = data_transform(img)
# 增加Batch
img = torch.unsqueeze(img, dim=0)

# read class_indict
try:
    json_file = open('./class_indices.json', 'r')
    class_indict = json.load(json_file)
except Exception as e:
    print(e)
    exit(-1)

# 创建一个模型
model = vgg(model_name="vgg16", num_classes=5)
# 得到最佳的模型的权重表路径
model_weight_path = "./vgg16Net.pth"
# 加载最佳的模型的权重表
model.load_state_dict(torch.load(model_weight_path))
# 关闭dropout
model.eval()
with torch.no_grad():
    # 得到训练结果
    output = torch.squeeze(model(img))
    # 得到训练结果的概率大小
    predict = torch.softmax(output, dim=0)
    # 得到最大概率的下标
    predict_cla = torch.argmax(predict).numpy()
# 输出最大预测的类别名字
print(class_indict[str(predict_cla)])
plt.show()
```

​       